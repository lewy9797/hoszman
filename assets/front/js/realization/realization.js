function getPhotos(id){
	let galleriesHTML = document.getElementsByClassName('realization-gallery');
	let buttonsHTML = document.getElementsByClassName('type-button');
	for(let i=0 ;i<galleriesHTML.length ; i++){
		if(parseInt(galleriesHTML[i].id) != parseInt(id)){
			galleriesHTML[i].classList.add('d-none');
			if(buttonsHTML[i].classList.contains('active')) buttonsHTML[i].classList.remove('active');
		} else{
			galleriesHTML[i].classList.remove('d-none');
			buttonsHTML[i].classList.add('active');
		}
	}
}

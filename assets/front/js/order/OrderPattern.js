
	function addOrderPattern(){
		let orderPatterns = document.querySelector('#order-patterns');
		const orderPattern = document.querySelector('.order-pattern');
		let newPattern = orderPattern;
		const orderPatternAmount = orderPatterns.getElementsByClassName('order-pattern').length;

		let inputValues = getInputValues(orderPatterns.getElementsByClassName('order-pattern'));


		orderPatterns.innerHTML = orderPatterns.innerHTML + `<div id="order-pattern-${parseInt(document.querySelector('#patterns_number').value) + 1}" class="row mb-4 order-pattern d-flex justify-content-between">${newPattern.innerHTML}</div>`;

		setInputValues(inputValues);

		setPatternsNumber();
		
		
		let inputs = document.querySelector('#order-pattern-'+(orderPatternAmount+1).toString()).getElementsByClassName('order-pattern-input');


		inputs = setInputsClass(inputs, orderPatternAmount+1);
	}

	function setInputsClass(inputs, formNumber){
		for(let i=0 ; i< inputs.length ; i++){
			let id = inputs[i].id;
			let className = id.split('_');
			className[className.length-1] = formNumber;
			inputs[i].classList.remove(id);
			inputs[i].classList.add(className.join('_'));
			inputs[i].id = className.join('_');
			inputs[i].name = className.join('_');
		}

		return inputs;
	}

	function setPatternsNumber(){
		document.querySelector('#patterns_number').value = parseInt(document.querySelector('#patterns_number').value)+1;
	}

	function getInputValues(orderPatterns){
		let inputValues = [];
		for(let i=0 ; i < orderPatterns.length ; i++){
			let fields = orderPatterns[i].getElementsByClassName('single-field');
			let singleValues = [];
			for(let j=0 ; j<fields.length ; j++) singleValues.push(fields[j].getElementsByClassName('order-pattern-input')[0].value);
				inputValues.push(singleValues);
		}
		return inputValues;
	}

	function setInputValues(inputValues){
		for(let i=0 ; i < inputValues.length ; i++){
			for(let j = 0 ; j < inputValues[i].length ; j++){
				document.getElementsByClassName('order-pattern')[i].getElementsByClassName('single-field')[j].getElementsByClassName('order-pattern-input')[0].value = inputValues[i][j];
			}
		}
	}

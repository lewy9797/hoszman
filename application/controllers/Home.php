<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->db->table_exists('users')){
			$this->base_m->create_base();
		}
		if(!isset($_SESSION['lang'])) $_SESSION['lang'] = 'pl';
	}


	public function test(){
		$base_folder = 'C:\Users\Medion\Desktop\klienci stron dedykowanych\hoszman\materiały klienta 1 — kopia';
		$directories = preg_grep('/^([^.])/', scandir($base_folder));
		foreach($directories as $dir){
			$sub_dirs = preg_grep('/^([^.])/', scandir($base_folder. '\\'. $dir));
			$i=0;
			foreach($sub_dirs as $sub){
				$photos = preg_grep('/^([^.])/', scandir($base_folder. '\\'. $dir. '\\'. (string)($i+1)));
				$insert['title'] = $sub;
				$insert['realization_type_id'] = explode('_', $dir)[1];
				addToSingleRealizations($insert);
				$item_id = $this->back_m->get_last('single_realizations')[0];
				foreach($photos as $photo){
					$insert['full_path'] = $base_folder. '\\'. $dir . '\\'. $sub. '\\'. $photo;
					$insert['name'] = $photo;
					if(file_exists($insert['full_path'])){
						$insert = upload($insert);
						$insert['item_id'] = $item_id->id;
						addToGallery($insert);

					}

				}
				$i++;
			}
		}
	}

	public function miniatura(){
		$gallery = $this->back_m->get_all('gallery');
		foreach($gallery as $photo){

			if(strpos(strtolower($photo->photo), 'miniatura') !== false){
				$insert['photo'] = $photo->photo;
				if($this->back_m->get_one('single_realizations', $photo->item_id)){

					$this->back_m->update('single_realizations', $insert, $photo->item_id);
				}
			}
		}
	}

	public function index() {
		$data = loadDefaultDataFront();
		$data['slider'] = $this->back_m->get_all('slider');
		$data['attributes'] = $this->back_m->get_one('attributes',1);
		$data['parallax'] = $this->back_m->get_one('parallax',1);
		$data['production'] = $this->back_m->get_all('production');
		$data['realizations'] = $this->back_m->get_all('realizations');
		$data['realization_attributes'] = $this->back_m->get_one('realization_attributes',1);

		echo loadViewsFront('index', $data);
	}

	public function cooperation(){
		$data = loadDefaultDataFront();
		$data['cooperation'] = $this->back_m->get_one('cooperation', 1);
		echo loadViewsFront('cooperation', $data);
	}

	public function contact(){
		$data = loadDefaultDataFront();
		$data['contact_icons'] = $this->back_m->get_all('contact_icons');

		echo loadViewsFront('contact', $data);
	}

	public function change($lang)
	{
		$_SESSION['lang'] = $lang;
	}

	public function realizations(){
		$data = loadDefaultDataFront();
		$data['realizations'] = $this->back_m->get_all('realizations');
		echo loadViewsFront('realizations', $data);
	}

	public function realization($id){
		$data = loadDefaultDataFront();
		$data['realization'] = $this->back_m->get_one('realizations', $id);
		$data['realization_attributes'] = $this->back_m->get_one('realization_attributes', 1);
		$data['realization_types'] = $this->back_m->get_realization_types($id); 
		$data['single_realizations'] = [];
		foreach($data['realization_types'] as $type){
			foreach($this->back_m->get_all_where('single_realizations', 'realization_type_id', $type->id) as $single_realization){
				array_push($data['single_realizations'], $single_realization);
			}
		}

		echo loadViewsFront('realization', $data);
	}

	public function single_realization($category, $single_realization_id){
		$data = loadDefaultDataFront();
		$data['single_realization'] = $this->back_m->get_one('single_realizations', $single_realization_id);
		$data['gallery'] = $this->back_m->get_images('gallery', 'single_realizations', $single_realization_id);
		
		echo loadViewsFront('single_realization', $data);
	}

	public function order(){
		$data = loadDefaultDataFront();
		$data['order'] = $this->back_m->get_one('order_attributes', 1);

		echo loadViewsFront('order', $data);
	}

	public function about(){
		$data = loadDefaultDataFront();
		$data['about'] = $this->back_m->get_one('about', 1);
		$data['gallery'] = $this->back_m->get_images('gallery', 'about', 1);

		echo loadViewsFront('about', $data);
	}

	public function offers(){
		$data = loadDefaultDataFront();
		$data['offer'] = $this->back_m->get_one('offer', 1);
		$data['offers'] = $this->back_m->get_all('offerts');
		echo loadViewsFront('offers', $data);
	}

	public function offer($id){
		$data = loadDefaultDataFront();
		$data['offer'] = $this->back_m->get_one('offerts', $id);
		$data['offer_attributes'] = $this->back_m->get_one('offer',1);
		$data['photos'] = $this->back_m->get_images('gallery', 'offerts', $id);
		$data['wood_types'] = $this->back_m->get_all($id == 1 ? 'wood_types_door' :'wood_types_stairs');
		$data['realizations'] = $this->back_m->get_all('our_realizations');


		echo loadViewsFront('offer', $data);
	}

	public function order_stairs(){
		$data = loadDefaultDataFront();
		$data['stair_projects'] = $this->back_m->get_all('stair_projects');
		$data['stair_constructions'] = $this->back_m->get_all('stair_constructions');
		$data['stair_parameters'] = $this->back_m->get_all('stair_parameters');
		$data['step_fulfillments'] = $this->back_m->get_all('step_fulfillments');
		$data['balustrade_fulfillments'] = $this->back_m->get_all('balustrade_fulfillments');
		$data['wood_types'] = $this->back_m->get_all('wood_types_stairs');
		$data['stair_finishes'] = $this->back_m->get_all('stair_finishes');
		$data['order_attributes'] = $this->back_m->get_one('order_attributes', 2);
		$data['upper_balustrade_length'] = $this->back_m->get_one('upper_balustrade_length', 1);
		$data['roof_covering'] = $this->back_m->get_one('roof_covering', 1);
		$data['patterns'] = $this->back_m->get_all('patterns');

		echo loadViewsFront('order_stairs', $data);
	}

	public function order_door(){
		$data = loadDefaultDataFront();
		$data['door_projects'] = $this->back_m->get_all('door_projects');
		$data['wood_types'] = $this->back_m->get_all('wood_types_door');
		$data['order_attributes'] = $this->back_m->get_all('order_attributes');
		$data['door_parameters'] = $this->back_m->get_all('door_parameters');
		$data['stair_finishes'] = $this->back_m->get_all('stair_finishes');

		echo loadViewsFront('order_door', $data);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_doors_mailer extends CI_Controller {

    function __construct(){
        parent::__construct();
        if ($this->input->post('secret_key') == $this->back_m->get_one('settings',1)->captcha_secret){

        }else{
            $this->session->set_flashdata('error', 'Coś poszło nie tak, przepraszamy za utrudnienia!');
            redirect('wycena/drzwi');
        }
    }

    public function send() {
        $now = date('Y-m-d');
        if (!is_dir('mailer/attachment/'.$now)) {
            mkdir('./mailer/attachment/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './mailer/attachment/'.$now;
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if($_FILES['attachment'] != null) {
            if ($this->upload->do_upload('attachment')) {
                $data = $this->upload->data();
                $_POST['attachment'] = $now.'/'.$data['file_name'];        
            }
        }      
        print_r($_POST);
        
        $data['contact'] = $this->back_m->get_one('contact_settings', 1);

        $insert['name'] = $this->input->post('name');
        $insert['email'] = $this->input->post('email');
        $insert['subject'] = 'Drzwi - formularz zamówienia';
        $insert['phone'] = $this->input->post('phone');
        $insert['address'] = $this->input->post('zip_code').' '.$this->input->post('city');
        $insert['attachment'] = $_POST['attachment'];
        
        for($i=0 ; $i<$this->input->post('patterns_number') ; $i++){

            $insert['wood_type'] = $this->input->post('wood_type_'.(string)($i+1));
            $insert['order_pattern'] = $this->input->post('order_pattern_'.(string)($i+1));
            $insert['door_width'] = $this->input->post('door_width_'.(string)($i+1));
            $insert['door_height'] = $this->input->post('door_height_'.(string)($i+1));
            $insert['door_amount'] = $this->input->post('door_amount_'.(string)($i+1));
            $insert['wall_thickness'] = $this->input->post('wall_thickness_'.(string)($i+1));
            $insert['finish'] = $this->input->post('finish_'.(string)($i+1));
            $insert['type'] = 'door';


            if($this->input->post('rodo1') != null ) { 
                $insert['rodo1'] = '1'; 
                $_POST['rodo1'] = 'Zaakceptowane';
            } else {
                $insert['rodo1'] = '0';
                $_POST['rodo1'] = 'Niezaakceptowane';
            }
            if($this->input->post('rodo2') != null ) { 
                $insert['rodo2'] = '1'; 
                $_POST['rodo2'] = 'Zaakceptowane';
            } else {
                $insert['rodo2'] = '0';
                $_POST['rodo2'] = 'Niezaakceptowane';
            }
            $this->back_m->insert('orders', $insert);

        }

        require 'application/libraries/mailer/config.php';
        require 'application/libraries/mailer/functions.php';
        require 'application/libraries/mailer/PHPMailerAutoload.php';

        $_POST['base_url'] = base_url(); 
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = $cfg['smtp_host'];
        $mail->SMTPAuth = true;         
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->Username = $cfg['smtp_user'];
        $mail->Password = $cfg['smtp_pass'];
        $mail->Port = $cfg['smtp_port'];
        $mail->setFrom($cfg['smtp_user'], $data['contact']->company .  ' - formularz zgłoszeniowy');
        $mail->addAttachment('mailer/attachment/'.$_POST['attachment']);
        $mail->AddBCC($data['contact']->email1);
        if(!empty($_POST['email'])) {
            $mail->addReplyTo($_POST['email']);
        }
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = $data['contact']->company .  ' - formularz zgłoszeniowy';
        $mail->Body = build_order_body($_POST, order_door_body($_POST));
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            exit;
        } else {
            $this->session->set_flashdata('success', '<p class="text-success font-weight-bold mb-0">Pomyślnie wysłałeś zgłoszenie!</p>');
            redirect('wycena/drzwi');

        }
        
        
    }
}










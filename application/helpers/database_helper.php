<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function upload($photo){
	$now = date('Y-m-d');
	if (!is_dir('uploads/'.$now)) {
		mkdir('./uploads/' . $now, 0777, TRUE);
	}
	$photo['name'] = rand(). '_'. $photo['name'];
	$photo['upload_path'] = $now.'/'.$photo['name'];
	copy($photo['full_path'], 'C:\xampp\htdocs\hoszman\uploads/'.  $photo['upload_path']);
	return prepareData($photo);

}

function prepareData($photo){
	$photo['title'] = explode('\\', $photo['full_path'])[8];
	return $photo;
}

function addToSingleRealizations($photo){
	$CI =& get_instance();
	unset($photo['full_path']);
	unset($photo['name']);
	unset($photo['upload_path']);
	unset($photo['item_id']);
	$CI->back_m->insert('single_realizations', $photo);
}

function addToGallery($photo){
	$CI =& get_instance();
	$insert['photo'] = $photo['upload_path'];
	$insert['table_name'] = 'single_realizations';
	$insert['item_id'] = $photo['item_id'];
	$CI->back_m->insert('gallery', $insert);
}
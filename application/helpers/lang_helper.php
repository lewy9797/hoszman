<?php 

function getTranslation($row, $field){
	$row = get_object_vars($row);
	return $_SESSION['lang'] == 'pl' ? $row[$field] : $row[$_SESSION['lang']. '_'. $field];
}
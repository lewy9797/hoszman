<?php

function build_mail_body($post, $template){
	$content = file_get_contents('application/libraries/mailer/templates/'.$template);
	foreach($post as $k => $v){
		$content = str_replace('{'.$k.'}', $v, $content);
	}

	return $content;
}

function build_order_body($post, $content){
	foreach($post as $k => $v){
		$content = str_replace('{'.$k.'}', $v, $content);
	}

	return $content;
}

function order_door_body($post){
	$body = '<div><h3>Nowe zamówienie ze strony <a href="{base_url}">{base_url}</a></h3>
	<h3 style="font-weight: bold;">Dane kontaktowe:</h3>
	<p>Imię i nazwisko: {name}</p>
	<p>Adres e-mail: {email}</p>
	<p>Telefon: {phone}</p>
	<p>Miasto: {city}</p>
	<p>Kod Pocztowy: {zip_code}</p>
	<p>Przetwarzanie danych: {rodo1}</p>
	<p>Zgoda na kontakt telefoniczny lub email: {rodo2}</p><br>';

	for($i=0 ; $i<$post['patterns_number'] ; $i++){
		$body .= '<h3 style="font-weight: bold;">Wzór nr: '.(string)($i+1).'</h3><br>
		<p>Wzór drzwi: {order_pattern_'.(string)($i+1).'}</p>
		<p>Grubość ściany: {wall_thickness_'.(string)($i+1).'} cm</p>
		<p>Szerokość drzwi: {door_width_'.(string)($i+1).'} cm</p>
		<p>Wysokość drzwi: {door_height_'.(string)($i+1).'} cm</p>
		<p>Zamawiana ilość: {door_amount_'.(string)($i+1).'} szt.</p>
		<p>Drewno: {wood_type_'.(string)($i+1).'}</p>
		<p>Wykończenie: {finish_'.(string)($i+1).'}</p><br></div>';
	}

	return $body;
}

function order_stairs_body($post){
	$body = '<div><h3>Nowe zamówienie ze strony <a href="{base_url}">{base_url}</a></h3>
	<h3 style="font-weight: bold;">Dane kontaktowe:</h3>
	<p>Imię i nazwisko: {name}</p>
	<p>Adres e-mail: {email}</p>
	<p>Telefon: {phone}</p>
	<p>Miasto: {city}</p>
	<p>Kod Pocztowy: {zip_code}</p>
	<p>Przetwarzanie danych: {rodo1}</p>
	<p>Zgoda na kontakt telefoniczny lub email: {rodo2}</p><br>

	<p>Rodzaj drewna: {wood_type}</p>
	<p>Typ schodów: {stair_project}</p>
	<p>Konstrukcja schodów: {stair_construction}</p>
	<p>Wysokość do sufitu: {ceiling_height} cm</p>
	<p>Wysokość całkowita: {overall_height} cm</p>
	<p>Szerokość schodów: {stair_width} cm</p>';
	if($post['lifts'] != '') $body .= '<p>Liczba podniesień w schodach: {lifts}</p>';
	$body .= '<p>Wypełnienie biegu: {step_fulfillment}</p>
	<p>Wypełnienie balustrady: {balustrade_fulfillment}</p>
	<p>Długość balustrady górnej: {upper_balustrade_length} cm</p>
	<p>Obudowa stropu: {roof_covering} cm</p>
	<p>Wykończenie schodów: {stair_finish}</p>';

	return $body;
}



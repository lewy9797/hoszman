<h3>Nowe zamówienie ze strony <a href="{base_url}">{base_url}</a></h3>
<h3 style="font-weight: bold;">Dane kontaktowe:</h3>
<p>Imię i nazwisko: {name}</p>
<p>Adres e-mail: {email}</p>
<p>Telefon: {phone}</p>
<p>Miasto: {city}</p>
<p>Kod Pocztowy: {zip_code}</p>
<p>Przetwarzanie danych: {rodo1}</p>
<p>Zgoda na kontakt telefoniczny lub email: {rodo2}</p><br>

<p>Rodzaj drewna: {wood_type}</p>
<p>Typ schodów: {stair_project}</p>
<p>Konstrukcja schodów: {stair_construction}</p>
<p>Wysokość do sufitu: {ceiling_height} cm</p>
<p>Wysokość całkowita: {overall_height} cm</p>
<p>Szerokość schodów: {stair_width} cm</p>
<p>Liczba podniesień w schodach: {lifts}</p>
<p>Wypełnienie biegu: {step_fulfillment}</p>
<p>Wypełnienie balustrady: {balustrade_fulfillment}</p>
<p>Długość balustrady górnej: {upper_balustrade_length} cm</p>
<p>Obicie stropu: {roof_covering} cm</p>
<p>Wykończenie schodów: {stair_finish} cm</p>





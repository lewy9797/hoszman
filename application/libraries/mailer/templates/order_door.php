<h3>Nowe zamówienie ze strony <a href="{base_url}">{base_url}</a></h3>
<h3 style="font-weight: bold;">Dane kontaktowe:</h3>
<p>Imię i nazwisko: {name}</p>
<p>Adres e-mail: {email}</p>
<p>Telefon: {phone}</p>
<p>Miasto: {city}</p>
<p>Kod Pocztowy: {zip_code}</p>
<p>Przetwarzanie danych: {rodo1}</p>
<p>Zgoda na kontakt telefoniczny lub email: {rodo2}</p><br>

<p>Wzór drzwi: {order_pattern}</p>
<p>Grubość ściany: {wall_thickness} cm</p>
<p>Szerokość drzwi: {door_width} cm</p>
<p>Wysokość drzwi: {door_height} cm</p>
<p>Zamawiana ilość: {door_amount} szt.</p>
<p>Drewno: {wood_type}</p>
<p>Wykończenie: {finish}</p>

<h3 style="font-weight: bold">Załącznik: </h3>
<p>{attachment}</p>





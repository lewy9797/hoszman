<!---------------------------------------------------- First Modal ------------------------------------------------->
            <div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
              aria-hidden="true">

              <!-- Change class .modal-sm to change the size of the modal -->
              <div class="modal-dialog modal-lg" role="document">


                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title w-100 text-dark" id="myModalLabel_1">Zdjęcie</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab_1" role="tablist">
              <li class="nav-item nav-item-modal">
                <a class="nav-link active nav-link-modal" id="home-tab_1" data-toggle="tab" href="#home_1" role="tab" aria-controls="home"
                  aria-selected="true">Dodaj nowe zdjęcie: </a>
              </li>
              <li class="nav-item nav-item-modal">
                <a class="nav-link nav-link-modal" id="profile-tab_1" data-toggle="tab" href="#profile_1" role="tab" aria-controls="profile"
                  aria-selected="false">Wybierz: </a>
              </li>
              
            </ul>
            <div class="tab-content" id="myTabContent_1">
              <div class="tab-pane add-file-modal fade show active" id="home_1"  role="tabpanel" aria-labelledby="home-tab">
                <label class="custom-file m-3">
                    <input type="file" id="photo_1" onchange="photoListener(1)" class="custom-file-input" name="photo_1">
                    <span class="custom-file-control custom-file-control-inverse"></span>
                </label>
              </div>
              <div class="tab-pane fade" id="profile_1" role="tabpanel" aria-labelledby="profile-tab">
               <div class="gallery_box p-4">
                <input type="hidden" name="server_photo_1" id="server_photo_1" >
                <div class="modal-picture1"></div> 
              </div>
              
            </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Zapisz zmiany</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
<!---------------------------------------------------- Second Modal------------------------------------------------->
            <div class="modal fade" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
              aria-hidden="true">

              <!-- Change class .modal-sm to change the size of the modal -->
              <div class="modal-dialog modal-lg" role="document">


                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title w-100 text-dark" id="myModalLabel_2">Zdjęcie w tle</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab_2" role="tablist">
              <li class="nav-item">
                <a class="nav-link nav-link-modal active" id="home-tab_2" data-toggle="tab" href="#home_2" role="tab" aria-controls="home"
                  aria-selected="true">Dodaj nowe zdjęcie: </a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-modal" id="profile-tab_2" data-toggle="tab" href="#profile_2" role="tab" aria-controls="profile"
                  aria-selected="false">Wybierz: </a>
              </li>
              
            </ul>
            <div class="tab-content" id="myTabContent_2">
              <div class="tab-pane add-file-modal fade show active" id="home_2"  role="tabpanel" aria-labelledby="home-tab">
                <label class="custom-file m-3">
                    <input type="file" id="photo_2" onchange="photoListener(2)" class="custom-file-input" name="photo_2">
                    <span class="custom-file-control custom-file-control-inverse"></span>
                </label>
              </div>
              <div class="tab-pane fade" id="profile_2" role="tabpanel" aria-labelledby="profile-tab">
               <div class="gallery_box p-4">
                <input type="hidden" name="server_photo_2" id="server_photo_2" >
                    <div class="modal-picture2"></div>
                    
                    
              </div>
              
            </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Zapisz zmiany</button>
                  </div>
                </div>
              </div>
            </div>
    </div>
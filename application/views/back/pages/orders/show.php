    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5">Wiadomości #<?php echo $value->id; ?></h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
    </div><!-- d-flex -->

    <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
            <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>

        <div class="form-layout form-layout-2">

            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-b-0-force">
                                <label class="form-control-label">Imię i nazwisko:</label>
                                <input class="form-control" type="text" value="<?php echo $value->name; ?>" readonly>
                            </div>
                        </div><!-- col-12 -->
                        <div class="col-md-6 pr-0">
                            <div class="form-group">
                                <label class="form-control-label">Adres E-mail:</label>
                                <input id="email" class="form-control" type="text" value="<?php echo $value->email; ?>" readonly>
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-6 px-0">
                            <div class="form-group bd-l-0-force">
                                <label class="form-control-label">Telefon:</label>
                                <input class="form-control" type="text" value="<?php echo $value->phone; ?>" readonly>
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force">
                                <label class="form-control-label">Temat:</label>
                                <input class="form-control" type="text" value="<?php echo $value->subject; ?>" readonly>
                            </div>
                        </div><!-- col-12 -->
                    </div> <!-- set -->
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force">
                                <label class="form-control-label">Rodzaj drewna:</label>
                                <input class="form-control" type="text" value="<?php echo $value->wood_type; ?>" readonly>
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <?php if($value->type == 'stairs'): ?>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-t-0-force ">
                                    <label class="form-control-label">Typ schodów:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->stair_project; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-t-0-force ">
                                    <label class="form-control-label">Konstrukcja schodów:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->stair_construction; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-6 pr-0">
                                <div class="form-group bd-t-0-force bd-r-0-force">
                                    <label class="form-control-label">Wysokość do sufitu:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->ceiling_height. ' '. $order_attributes[1]->ceiling_height_unit; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-6 px-0">
                                <div class="form-group bd-t-0-force ">
                                    <label class="form-control-label">Wysokość całkowita:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->overall_height. ' '. $order_attributes[1]->overall_height_unit; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row"> <!-- set -->
                            <div class="col-md-3 pr-0">
                                <div class="form-group bd-t-0-force bd-r-0-force">
                                    <label class="form-control-label">Szerokość Schodów:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->stair_width.' '. $order_attributes[1]->stair_width_unit; ; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->

                            <div class="col-md-3 px-0">
                                <div class="form-group bd-t-0-force bd-r-0-force">
                                    <label class="form-control-label">Liczba <br>podniesień:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->lifts; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-t-0-force  bd-r-0-force">
                                    <label class="form-control-label">Długość bal. górnej:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->upper_balustrade_length.' '. $order_attributes[1]->upper_balustrade_length_unit; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-t-0-force">
                                    <label class="form-control-label">Obudowa stropu:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->roof_covering.' '. $order_attributes[1]->roof_covering_unit; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div> <!-- set -->
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-t-0-force">
                                    <label class="form-control-label">Wypełnienie biegu:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->step_fulfillment; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-t-0-force">
                                    <label class="form-control-label">Wypełnienie balustrady:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->balustrade_fulfillment; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-t-0-force bd-b-0-force">
                                    <label class="form-control-label">Wykończenie schodów:</label>
                                    <input class="form-control" type="text" value="<?php echo $value->finish; ?>" readonly>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <?php else: ?>
                            <div class="row"> <!-- set -->
                                <div class="col-md-4 pr-0">
                                    <div class="form-group bd-t-0-force bd-r-0-force">
                                        <label class="form-control-label">Grubość Ściany:</label>
                                        <input class="form-control" type="text" value="<?php echo $value->wall_thickness.' '. $order_attributes[0]->wall_thickness_unit ; ?>" readonly>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group bd-t-0-force bd-r-0-force">
                                        <label class="form-control-label">Szerokość drzwi:</label>
                                        <input class="form-control" type="text" value="<?php echo $value->door_width.' '. $order_attributes[0]->door_width_unit; ?>" readonly>
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group bd-t-0-force">
                                        <label class="form-control-label">Wysokość drzwi:</label>
                                        <input class="form-control" type="text" value="<?php echo $value->door_height.' '. $order_attributes[0]->door_height_unit; ?>" readonly>
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row">
                                <div class="col-md pr-0">
                                    <div class="form-group bd-t-0-force  ">
                                        <label class="form-control-label">Zamawiana Ilość:</label>
                                        <input class="form-control" type="text" value="<?php echo $value->door_amount; ?>" readonly>
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row">
                                <div class="col-md pr-0">
                                    <div class="form-group bd-t-0-force bd-b-0-force">
                                        <label class="form-control-label">Projekt drzwi:</label>
                                        <input class="form-control" type="text" value="<?php echo $value->order_pattern; ?>" readonly>
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                        <?php endif; ?>
                        <div class="row"> <!-- set -->
                            <div class="col-md-6 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="ckbox">
                                        <input type="checkbox" <?php if($value->rodo1 == 1){echo 'checked';} ?> onclick="return false;">
                                        <span>Zgoda na przetwarzanie danych osobowych</span>
                                    </label>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-6 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="ckbox">
                                        <input type="checkbox" <?php if($value->rodo2 == 1){echo 'checked';} ?> onclick="return false;">
                                        <span>Zgoda na kontakt mailowy i telefoniczny</span>
                                    </label>
                                </div>
                            </div><!-- col-4 -->
                            <?php if(!empty($value->attachment)):?>
                                <div class="col-md-12 pr-0">
                                    <div class="form-group bd-b-0-force">
                                        <label class="form-control-label">Załącznik do pobrania: <a download href="<?php echo base_url(); ?>mailer/attachment/<?php echo $value->attachment?>"><i class="fas fa-file-download fa-3x ml-3 tx-black"></i></a></label>

                                    </div>
                                </div><!-- col-4 -->
                            <?php endif; ?>
                        </div> <!-- set -->
                        <div class="row">
                            <div class="col-md-12 pr-0">
                                <div class="form-layout-footer bd pd-20">
                                    <button class="btn btn-secondary" onclick="window.history.go(-1); return false;">Powrót</button>
                                </div><!-- form-group -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group bd-l-0-force">
                                <div id="answer">
                                    <?php if($value->answer == 1): ?>
                                        <span class="text-success"><i class="fas fa-check"></i> Odpowiedź została wysłana!</span>
                                    <?php endif; ?>
                                </div>
                                <label class="form-control-label">Temat Twojej wiadomości:</label>
                                <input class="form-control" type="text" name="subject" required>
                            </div>
                        </div><!-- col-12 -->
                        <div class="col-md-12">
                            <div class="form-group bd-l-0-force bd-t-0-force">
                                <label class="form-control-label">Treść Twojej wiadomości</label>
                                <textarea class="summernote" class="form-control" name="message"></textarea>
                            </div>
                        </div><!-- col-12 -->
                        <div class="col-md-12">
                            <div class="form-layout-footer bd pd-20 bd-l-0-force bd-t-0-force">
                                <button class="btn btn-info" onclick="sendMessage(<?php echo $value->id; ?>)">Wyślij</button>
                            </div><!-- form-group -->
                        </div><!-- col-12 -->
                    </div>
                </div><!-- row -->
            </div><!-- form-layout -->


            <script type="text/javascript">
              function sendMessage(id)
              {
                var id = id;
                var subject = document.getElementsByName('subject')[0].value;
                var message = document.getElementsByName('message')[0].value;
                var email = document.getElementById('email').value;
                $.ajax({  
                 type: "post", 
                 url:"<?php echo base_url(); ?>panel/orders/send", 
                 data: {id:id, subject:subject, message:message, email:email}, 
                 cache: false,
                 beforeSend:function(html)
                 {
                    document.getElementById('answer').innerHTML = '<p class=""><i class="fas fa-spinner fa-pulse loader"></i></p>';
                },
                complete:function(html)
                {
                 console.log(html);
                 document.getElementById('answer').innerHTML = '<p class="text-success"><i class="fas fa-check"></i> Odpowiedź została wysłana!</p>';
             }  
         });  
            }
        </script>
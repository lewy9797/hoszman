    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5"><?php echo ucfirst(str_replace('_', ' ', $this->uri->segment(2))); ?></h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
    </div><!-- d-flex -->

    <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
            <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>

        <form class="form-layout form-layout-2" action="<?php echo base_url(); ?>panel/<?php echo $this->uri->segment(2); ?>/action/<?php echo $this->uri->segment(4) . '/' . $this->uri->segment(2); ?>/<?php echo @$value->id; ?>" method="post"  enctype="multipart/form-data">

            <div class="row no-gutters">
                <div class="col-md">
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group">
                                <label class="form-control-label">Tytuł: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="title" value="<?php echo @$value->title; ?>" required>
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force bd-b-0-force ">
                                <label class="form-control-label">Opis:</label>
                                <textarea class="summernote" name="description"><?php echo @$value->description; ?></textarea>
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <?php if(@$value->id == 2): ?>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-r-0-force bd-b-0-force">
                                    <label class="form-control-label">Notka Informacyjna: </label>
                                    <input class="form-control" type="text" name="info" value="<?php echo @$value->info; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                    <?php endif; ?>
                    <div class="row"> <!-- set -->
                        <div class="col-md-6 pr-0">
                            <div class="form-group bd-r-0-force">
                                <label class="form-control-label">Tytuł Pierwszej Sekcji: </label>
                                <input class="form-control" type="text" name="first_title" value="<?php echo @$value->first_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-6 px-0">
                            <div class="form-group">
                                <label class="form-control-label">Tytuł Drugiej Sekcji: </label>
                                <input class="form-control" type="text" name="second_title" value="<?php echo @$value->second_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force bd-b-0-force">
                                <label class="form-control-label">Tytuł Trzeciej Sekcji: </label>
                                <input class="form-control" type="text" name="third_title" value="<?php echo @$value->third_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <?php if($value->id == 1): ?>

                        <div class="row">
                            <div class="col-md-12 pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tytuł zamówienia: </label>
                                    <input class="form-control" type="text" name="order_title" value="<?php echo @$value->order_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Wzór drzwi: </label>
                                    <input class="form-control" type="text" name="door_pattern" value="<?php echo @$value->door_pattern; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Grubość ściany: </label>
                                    <input class="form-control" type="text" name="order_wall_thickness" value="<?php echo @$value->order_wall_thickness; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Szerokość drzwi: </label>
                                    <input class="form-control" type="text" name="order_door_width" value="<?php echo @$value->order_door_width; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Wysokość drzwi: </label>
                                    <input class="form-control" type="text" name="order_door_height" value="<?php echo @$value->order_door_height; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Ilość: </label>
                                    <input class="form-control" type="text" name="door_amount" value="<?php echo @$value->door_amount; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Drewno: </label>
                                    <input class="form-control" type="text" name="wood" value="<?php echo @$value->wood; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Wykończenie: </label>
                                    <input class="form-control" type="text" name="finish" value="<?php echo @$value->finish; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka grubości ściany: </label>
                                    <input class="form-control" type="text" name="wall_thickness_unit" value="<?php echo @$value->wall_thickness_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka szerokości drzwi: </label>
                                    <input class="form-control" type="text" name="door_width_unit" value="<?php echo @$value->door_width_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka wysokości drzwi: </label>
                                    <input class="form-control" type="text" name="door_height_unit" value="<?php echo @$value->door_height_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Jednostka Ilości: </label>
                                    <input class="form-control" type="text" name="door_amount_unit" value="<?php echo @$value->door_amount_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Dodaj kolejny wzór do zamówienia: </label>
                                    <input class="form-control" type="text" name="pattern_title" value="<?php echo @$value->pattern_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <?php else: ?>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Czwartej Sekcji: </label>
                                        <input class="form-control" type="text" name="fourth_title" value="<?php echo @$value->fourth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row">
                                <div class="col-md-4 pr-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka wysokości do sufitu: </label>
                                        <input class="form-control" type="text" name="ceiling_height_unit" value="<?php echo @$value->ceiling_height_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka wysokości całkowitej: </label>
                                        <input class="form-control" type="text" name="overall_height_unit" value="<?php echo @$value->overall_height_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Jednostka szerokości schodów: </label>
                                        <input class="form-control" type="text" name="stair_width_unit" value="<?php echo @$value->stair_width_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row">
                                <div class="col-md pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tekst pola dla liczby podniesień: </label>
                                        <input class="form-control" type="text" name="lifts" value="<?php echo @$value->lifts; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Piątej Sekcji: </label>
                                        <input class="form-control" type="text" name="fifth_title" value="<?php echo @$value->fifth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Szóstej Sekcji: </label>
                                        <input class="form-control" type="text" name="sixth_title" value="<?php echo @$value->sixth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tekst na przycisku Wzorów Tralek i słupów: </label>
                                        <input class="form-control" type="text" name="pattern_button_name" value="<?php echo @$value->pattern_button_name; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Siódmej Sekcji: </label>
                                        <input class="form-control" type="text" name="seventh_title" value="<?php echo @$value->seventh_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Ósmej Sekcji: </label>
                                        <input class="form-control" type="text" name="eighth_title" value="<?php echo @$value->eighth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka długości górnej balustrady: </label>
                                        <input class="form-control" type="text" name="upper_balustrade_length_unit" value="<?php echo @$value->upper_balustrade_length_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-6 px-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Jednostka obudowy stropu: </label>
                                        <input class="form-control" type="text" name="roof_covering_unit" value="<?php echo @$value->roof_covering_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Dziewiątej Sekcji: </label>
                                        <input class="form-control" type="text" name="ninth_title" value="<?php echo @$value->ninth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tytuł Formularza: </label>
                                    <input class="form-control" type="text" name="form_title" value="<?php echo @$value->form_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Opis Formularza: </label>
                                    <textarea class="summernote" name="form_description"><?php echo @$value->form_description; ?></textarea>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Imię i nazwisko: </label>
                                    <input class="form-control" type="text" name="name" value="<?php echo @$value->name; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">E-mail: </label>
                                    <input class="form-control" type="text" name="email" value="<?php echo @$value->email; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Telefon: </label>
                                    <input class="form-control" type="text" name="phone" value="<?php echo @$value->phone; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Miasto: </label>
                                    <input class="form-control" type="text" name="city" value="<?php echo @$value->city; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Kod Pocztowy: </label>
                                    <input class="form-control" type="text" name="zip_code" value="<?php echo @$value->zip_code; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Wiadomość: </label>
                                    <input class="form-control" type="text" name="message" value="<?php echo @$value->message; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tekst na przycisku z plikiem: </label>
                                    <input class="form-control" type="text" name="file_button_name" value="<?php echo @$value->file_button_name; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tekst na przycisku formularza: </label>
                                    <input class="form-control" type="text" name="form_button_name" value="<?php echo @$value->form_button_name; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>

                        <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group">
                                <label class="form-control-label">Tytuł (j.niemiecki): </label>
                                <input class="form-control" type="text" name="de_title" value="<?php echo @$value->de_title; ?>" >
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force bd-b-0-force ">
                                <label class="form-control-label">Opis (j.niemiecki):</label>
                                <textarea class="summernote" name="de_description"><?php echo @$value->de_description; ?></textarea>
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <?php if(@$value->id == 2): ?>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-r-0-force bd-b-0-force">
                                    <label class="form-control-label">Notka Informacyjna (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_info" value="<?php echo @$value->de_info; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                    <?php endif; ?>
                    <div class="row"> <!-- set -->
                        <div class="col-md-6 pr-0">
                            <div class="form-group bd-r-0-force">
                                <label class="form-control-label">Tytuł Pierwszej Sekcji (j.niemiecki): </label>
                                <input class="form-control" type="text" name="de_first_title" value="<?php echo @$value->de_first_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-6 px-0">
                            <div class="form-group">
                                <label class="form-control-label">Tytuł Drugiej Sekcji (j.niemiecki): </label>
                                <input class="form-control" type="text" name="de_second_title" value="<?php echo @$value->de_second_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <div class="row"> <!-- set -->
                        <div class="col-md-12 pr-0">
                            <div class="form-group bd-t-0-force bd-b-0-force">
                                <label class="form-control-label">Tytuł Trzeciej Sekcji (j.niemiecki): </label>
                                <input class="form-control" type="text" name="de_third_title" value="<?php echo @$value->de_third_title; ?>">
                            </div>
                        </div><!-- col-4 -->
                    </div> <!-- set -->
                    <?php if($value->id == 1): ?>

                        <div class="row">
                            <div class="col-md-12 pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tytuł zamówienia (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_order_title" value="<?php echo @$value->de_order_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Wzór drzwi (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_door_pattern" value="<?php echo @$value->de_door_pattern; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Grubość ściany (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_order_wall_thickness" value="<?php echo @$value->de_order_wall_thickness; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Szerokość drzwi (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_order_door_width" value="<?php echo @$value->de_order_door_width; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Wysokość drzwi (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_order_door_height" value="<?php echo @$value->de_order_door_height; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Ilość (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_door_amount" value="<?php echo @$value->de_door_amount; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Drewno (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_wood" value="<?php echo @$value->de_wood; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Wykończenie (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_finish" value="<?php echo @$value->de_finish; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-3 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka grubości ściany (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_wall_thickness_unit" value="<?php echo @$value->de_wall_thickness_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka szerokości drzwi (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_door_width_unit" value="<?php echo @$value->de_door_width_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Jednostka wysokości drzwi (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_door_height_unit" value="<?php echo @$value->de_door_height_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-3 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Jednostka Ilości (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_door_amount_unit" value="<?php echo @$value->de_door_amount_unit; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Dodaj kolejny wzór do zamówienia (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_pattern_title" value="<?php echo @$value->de_pattern_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <?php else: ?>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Czwartej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_fourth_title" value="<?php echo @$value->de_fourth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row">
                                <div class="col-md-4 pr-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka wysokości do sufitu (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_ceiling_height_unit" value="<?php echo @$value->de_ceiling_height_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka wysokości całkowitej (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_overall_height_unit" value="<?php echo @$value->de_overall_height_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-4 px-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Jednostka szerokości schodów (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_stair_width_unit" value="<?php echo @$value->de_stair_width_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row">
                                <div class="col-md pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tekst pola dla liczby podniesień (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_lifts" value="<?php echo @$value->de_lifts; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Piątej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_fifth_title" value="<?php echo @$value->de_fifth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Szóstej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_sixth_title" value="<?php echo @$value->de_sixth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tekst na przycisku Wzorów Tralek i słupów: </label>
                                        <input class="form-control" type="text" name="de_pattern_button_name" value="<?php echo @$value->de_pattern_button_name; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Siódmej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_seventh_title" value="<?php echo @$value->de_seventh_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Ósmej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_eighth_title" value="<?php echo @$value->de_eighth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                            <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group  bd-b-0-force bd-r-0-force">
                                        <label class="form-control-label">Jednostka długości górnej balustrady (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_upper_balustrade_length_unit" value="<?php echo @$value->de_upper_balustrade_length_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                                <div class="col-md-6 px-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Jednostka obudowy stropu (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_roof_covering_unit" value="<?php echo @$value->de_roof_covering_unit; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div>
                            <div class="row"> <!-- set -->
                                <div class="col-md-12 pr-0">
                                    <div class="form-group  bd-b-0-force">
                                        <label class="form-control-label">Tytuł Dziewiątej Sekcji (j.niemiecki): </label>
                                        <input class="form-control" type="text" name="de_ninth_title" value="<?php echo @$value->de_ninth_title; ?>">
                                    </div>
                                </div><!-- col-4 -->
                            </div> <!-- set -->
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tytuł Formularza (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_form_title" value="<?php echo @$value->de_form_title; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Opis Formularza (j.niemiecki): </label>
                                    <textarea class="summernote" name="de_form_description"><?php echo @$value->de_form_description; ?></textarea>
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Imię i nazwisko (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_name" value="<?php echo @$value->de_name; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">E-mail (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_email" value="<?php echo @$value->de_email; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Telefon (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_phone" value="<?php echo @$value->de_phone; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Miasto (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_city" value="<?php echo @$value->de_city; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force bd-r-0-force">
                                    <label class="form-control-label">Kod Pocztowy (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_zip_code" value="<?php echo @$value->de_zip_code; ?>">
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-md-4 px-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Wiadomość (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_message" value="<?php echo @$value->de_message; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tekst na przycisku z plikiem (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_file_button_name" value="<?php echo @$value->de_file_button_name; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md pr-0">
                                <div class="form-group bd-b-0-force">
                                    <label class="form-control-label">Tekst na przycisku formularza (j.niemiecki): </label>
                                    <input class="form-control" type="text" name="de_form_button_name" value="<?php echo @$value->de_form_button_name; ?>">
                                </div>
                            </div><!-- col-4 -->
                        </div>
                        <div class="row">
                            <div class="col-md-12 pr-0">
                                <div class="form-layout-footer bd pd-20">
                                    <button class="btn btn-info" type="submit">Zapisz</button>
                                    <button class="btn btn-secondary" onclick="window.history.go(-1); return false;">Anuluj</button>
                                </div><!-- form-group -->
                            </div>
                        </div>
                    </div>
                </div><!-- row -->
          </form><!-- form-layout -->
<main class="index-main">
  <section class="slider">
    <div class="container">
      <div class="owl-carousel owl-theme">
        <?php foreach($slider as $slide): ?>
          <div class="item">
            <!-- <picture> -->
              <!-- <source data-srcset="<?= base_url().'uploads/'.$slide->photo ?>.webp" type="image/webp" class="lazy img-fluid"> -->
                <!-- <source data-srcset="<?= base_url().'uploads/'.$slide->photo ?>" type="image/jpeg" class="lazy img-fluid">  -->
                  <img data-src="<?= base_url().'uploads/'.$slide->photo ?>" alt="<?= getTranslation($slide, 'alt') ?>" class="lazy img-fluid">
                <!-- </picture> -->
                <div class="text-slider">
                  <!-- <div class="col-6 col-md-12 col-lg-12"> -->
                    <h2 class="header-slider mb-4 mb-lg-5"><?= getTranslation($slide, 'title') ?></h2>
                  <!-- </div> -->
                  <!-- <div class="col-6 col-md-12 col-lg-12"> -->
                    <div class="slider-body">
                     <?= getTranslation($slide, 'subtitle') ?>
                   </div>
                 <!-- </div> -->
                 
                 <a href="<?= $slide->link ?>">
                  <button class="btn btn-slider mt-3 mt-lg-5"><?= getTranslation($slide, 'button_name') ?> <i class="fas fa-chevron-right"></i></button>
                </a>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </section>
    <section class="py-5">
      <div class="container">
        <h2 class="heading text-center"><?= getTranslation($attributes, 'title')  ?></h2>

        <div class="pt-5 content-text"><?= getTranslation($attributes, 'description')  ?></div>
      </div>
    </section>
    <section>

      <div class="content-images lazy" style="background-image: url(<?= base_url(). 'uploads/'. $realization_attributes->photo ?>.webp)">
        <div class="triangle w-100 text-center">
          <img data-src="<?= base_url() ?>assets/front/img/triangle.png" class="lazy" alt="triangle">
          <img data-src="<?= base_url() ?>assets/front/img/arrow-down.png" alt="arrow" class=" lazy arrow-down">
        </div>
        <div class="container">
          <div class="row py-5 flex-lg-row flex-column">
            <?php $titles = ['schody', 'drzwi']; $i=0; foreach($realizations as $realization): ?>
            <a class="col-12 col-md-6 col-lg-6 gallery-box mx-auto py-2 py-lg-0" href="<?= base_url() . 'realizacje/'. $titles[$i] ?>" style="text-decoration: none; color:black">
              <div>
                <picture>
                  <source data-srcset="<?= base_url().'uploads/'.$realization->photo ?>.webp" type="image/webp" class="lazy img-fluid">
                    <source data-srcset="<?= base_url().'uploads/'.$realization->photo ?>" type="image/jpeg" class="lazy img-fluid"> 
                      <img data-src="<?= base_url() . 'uploads/' . $realization->photo ?>.webp" alt="<?= getTranslation($realization, 'alt') ?>" class="img-fluid lazy">
                    </picture>
                    <div class="image-text"><?= getTranslation($realization, 'title') ?></div>
                  </div>
                </a>
                <?php $i++; endforeach; ?>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="content-banner py-5 lazy" style="background-image: url('<?= base_url(). 'uploads/'. $parallax->photo ?>.webp')">
            <div class="container">
              <div class="row text-center py-5 m-0">
                <h2 class="heading-white text-center mx-auto text-shadow-1"><?= getTranslation($parallax, 'title') ?></h2>
                <div class="pt-5 content-text-white text-shadow-1"><?= getTranslation($parallax, 'description') ?></div>
              </div>
              <div class="w-100">
                <a href="<?= base_url() . $parallax->link ?>" style="color:black">
                  <button class="custom-btn btn mx-auto">
                    <?= getTranslation($parallax, 'button_name') ?>
                    <i class="fas fa-chevron-right">
                    </i>
                  </button>
                </a>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="container ">
            <div class="row flex-lg-row flex-column  justify-content-between align-items-center py-5">
              <?php $i=0; foreach($production as $product): ?>
              <div class="icon-wrapper d-flex flex-column align-items-center">
                <img data-src="<?= base_url() . 'uploads/' .$product->photo ?>.webp" alt="<?= getTranslation($product, 'alt') ?>" class="lazy img-fluid">
                <div class="icon-wrapper-text"><?= getTranslation($product, 'title')  ?></div>
              </div>
              <?php if($i != count($production)-1): ?>
                <img data-src="<?= base_url() ?>assets/front/img/arrow-right.png" alt="arrow" class="lazy img-fluid arrow-right">
              <?php endif; ?>
              <?php $i++; endforeach ?>

            </div>
          </div>

        </section>
      </main>
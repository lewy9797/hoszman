<main class="realizations-stairs-main">
	<section>
		<div class="container mb-5">
			<?php if($this->session->flashdata('success')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('success') ?></strong>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('error') ?></strong>                  
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>
			<h1 class="header-contact text-center"><?= getTranslation($contact[0], 'title') ?></h1>
			<div class="text-center py-3 contact-text">
				<?= getTranslation($contact[0], 'description') ?>
			</div>
			<div class="row py-5">
				<div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
					<div class="py-3 d-flex align-items-center">
						<div class="pr-3">
							<img data-src="<?= base_url(). 'uploads/'. $contact_icons[0]->photo ?>" class="lazy img-fluid" alt="<?= getTranslation($contact_icons[0], 'alt')  ?>">
						</div>
						<a href="https://www.google.com/maps/place/Hoszman.+Schody+i+drzwi/@51.5571783,15.5256733,17z/data=!4m13!1m7!3m6!1s0x4708ac3bfcebaf39:0x2f8a2578b835517a!2s<?= $contact_settings->address ?>,+<?= $contact_settings->zip_code ?>+<?= $contact_settings->city ?>!3b1!8m2!3d51.557175!4d15.527862!3m4!1s0x4708ac3bfcebaf39:0xcf11718392052b03!8m2!3d51.557175!4d15.527862" title="address"><?= $contact_settings->address ?>
					</br>
					<?= $contact_settings->zip_code . ' ' . $contact_settings->city ?>
				</a>
			</div>
			<div class="py-3 d-flex align-items-center">
				<div class="pr-3">
					<img data-src="<?= base_url(). 'uploads/'. $contact_icons[1]->photo ?>" class="img-fluid lazy" alt="<?= getTranslation($contact_icons[1], 'alt')  ?>">
				</div>
				<a href="mailto: <?= $contact_settings->email1 ?>" title="mail"><?= $contact_settings->email1 ?>

			</a>
		</div>
		<div class="py-3 d-flex align-items-center">
			<div class="pr-3">
				<img data-src="<?= base_url(). 'uploads/'. $contact_icons[2]->photo ?>" class="lazy img-fluid" alt="<?= getTranslation($contact_icons[2], 'alt') ?>">
			</div>
			<div class="d-flex flex-column">
				<a href="tel: <?= $contact_settings->phone1 ?>" title="number phone">
					Tel. <?= $contact_settings->phone1 ?>

				</a>
				<a href="tel: <?= $contact_settings->phone2 ?>" title="number phone">
					Tel. kom. <?= $contact_settings->phone2 ?>

				</a>
			</div>
		</div>
	</div>
	<div class="col-12 col-lg-6">
		<div class="mapouter">
			<div class="gmap_canvas text-left">
				<iframe width="500" height="500" id="gmap_canvas" src="<?= $contact_settings->map ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>
		</div>
	</div>
</div>
<div class="row justify-content-center py-5 p-3 p-lg-0">
	<h1 class="header-contact text-center"><?= getTranslation($contact[1], 'title') ?></h1>
	<div class="text-center py-3 contact-text">
		<?= getTranslation($contact[1], 'description') ?>
	</div>
	<!-- Default form register -->
	<form class="text-center w-100 p-3 p-lg-5" action="<?= base_url() . 'mailer/send' ?>" method="POST">
		<input type="hidden" name="secret_key" value="<?= $settings->captcha_secret ?>">
		<div class="form-row">
			<div class="col-12 col-lg-4 mb-4 mb-lg-0">
				<!-- First name -->
				<input required type="text" name="name" id="defaultRegisterFormFirstName" class="form-control" placeholder="<?= getTranslation($contact[2], 'name') ?>*">
			</div>
			<div class="col-12 col-lg-4">
				<!-- E-mail -->
				<input required type="email" name="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="<?= getTranslation($contact[2], 'email') ?>">
			</div>
			<div class="col-12 col-lg-4">
				<!-- E-mail -->
				<input required type="text" name="phone" id="defaultRegisterFormPhone" class="form-control mb-4" placeholder="<?= getTranslation($contact[2], 'phone') ?>">
			</div>
		</div>

		<textarea required id="textarea"name="message" class="form-control mb-4" placeholder="<?= getTranslation($contact[2], 'message') ?>"></textarea>



		<!-- Sign up button -->

		<!-- Newsletter -->
		<div class="custom-control custom-checkbox mb-2">
			<input type="checkbox" name="rodo1" class="custom-control-input" id="rodo1">
			<label class="custom-control-label text-left" for="rodo1">
				<?=  getTranslation($settings, 'rodo') ?>
			</label>
		</div>
		<div class="custom-control custom-checkbox">
			<input type="checkbox" name="rodo2" class="custom-control-input" id="rodo2">
			<label class="custom-control-label text-left" for="rodo2">
				<?= getTranslation($settings, 'rodo_tel') ?>
			</label>
		</div>

		<button class="btn btn-slider my-4" type="submit"><?= getTranslation($contact[2], 'button_name') ?> <i class="fas fa-chevron-right"></i></button>

	</form>
	<!-- Default form register -->
</div>

</div>
</section>
</main>
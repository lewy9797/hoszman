<main class="realizations-doors-main">

  <section>
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12 text-center">
          <div class="text-slider ">
            <h1 class="header-realizations offer-realizations mb-4 mb-lg-5 font-bold"><?= getTranslation($offer, 'title')  ?></h1>
            <div class="order-description pt-3">
              <div class="text-justify"><?= getTranslation($offer, 'description') ?></div>

              <div class="text-justify font-bold"><?= getTranslation($offer, 'short_description') ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
    <div class="container">
      <h2 class="text-center py-3"><?= getTranslation($offer_attributes, 'second_title')  ?></h2>

      <div class="row mt-4">
        <?php foreach($wood_types as $wood_type ): ?>
          <div class="col-md-4 col-12 mb-4 text-center">
            <img class="img-fluid" src="<?= base_url() . 'uploads/' . $wood_type->photo ?>" alt="<?= getTranslation($wood_type, 'alt') ?>">
            <h5 class="text-uppercase pt-3"><?= getTranslation($wood_type, 'title') ?></h5>
          </div>
        <?php endforeach; ?>


      </div>

    </div>

  </section>

  <section class="pt-3 pt-lg-5 pb-5 pb-lg-5 text-center">

    <div class="container">

      <h3 class="header-realizations offer-realizations mb-4 mb-lg-5 font-bold"><?= getTranslation($offer_attributes, 'third_title') ?></h3>

      <div class="row">

        <?php $i=1; foreach($realizations as $realization): ?>

          <div class="col-md-6 col-12 mb-4">

            <!-- Rotating card -->
            <div class="card-wrapper">
              <div id="card-<?= $i ?>" class="card card-rotating text-center">

                <a class="rotate-btn" data-card="card-<?= $i ?>">
                  <div class="face front offer-realization-photo" style="background: url('<?= base_url() . 'uploads/' . $realization->photo  ?>');">

                    <!-- Content -->
                    <div class="card-body card-rot">
                      <div class="card-rot-head">
                        <h4 class="font-weight-bold mb-3 text-uppercase text-white"><?= getTranslation($realization, 'title') ?></h4>
                      </div>
                    </div>

                  </div>
                </a>

                <!-- Back Side -->
                <div class="face back offer-realization-photo" style="background: url('<?= base_url() . 'uploads/' . $realization->photo  ?>');">

                  <a class="rotate-btn" data-card="card-<?= $i ?>">

                    <div class="card-body card-rot">

                      <div class="card-mask">

                        <!-- Content -->
                        <h4 class="font-weight-bold mb-0 pt-4 text-uppercase text-white"><?=  getTranslation($realization, 'title') ?></h4>

                        <div class="px-3 pt-3 text-white our-realization-description font-small"><?=  getTranslation($realization, 'description') ?></div>
  <?php $realization_link = $i == 2 ? base_url() . 'realizacje/schody' : base_url() . 'realizacje/drzwi'; ?>
                        <a href="<?= $realization_link ?>" class="font-bold text-white"><?= getTranslation($realization, 'button_name') ?> <i class="fas fa-angle-right"></i></a>

                      </div>

                    </div>

                  </a>

                </div>
                <!-- Back Side -->

              </div>
            </div>
            <!-- Rotating card -->

          </div>
        <?php $i++; endforeach; ?>

      </div>

    </div>

  </section>

</main>
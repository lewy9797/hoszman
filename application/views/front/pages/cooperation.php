 <main class="realizations-doors-main">

  <section>
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 d-flex flex-column justify-content-end">
          <div class="text-slider ">
            <h1 class="header-realizations mb-4 mb-lg-5 font-bold"><?= getTranslation($cooperation, 'title')  ?></h1>
            <div class="order-description">
             <?= getTranslation($cooperation, 'description') ?>
           </div>
         </div>
       </div>
       <div class="col-12 col-lg-6 py-3 py-lg-0">
        <img src="<?= base_url().'uploads/'.$cooperation->photo ?>" class="img-fluid" alt="<?= getTranslation($cooperation, 'alt') ?>">
      </div>
    </div>
  </div>
</section>

<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
  <div class="container">
    <h2 class="text-center py-3"><?= getTranslation($cooperation, 'form_title') ?></h2>


    <form action="<?= base_url() . 'mailer/send' ?>" method="POST">

      <div class="row">

        <div class="col-md-4 col-12 mb-4">
          <input type="text" id="name" class="form-control" name="name" placeholder="<?= getTranslation($contact[2], 'name') ?>*" required>
        </div>

        <div class="col-md-4 col-12 mb-4">
          <input type="text" id="email" class="form-control" name="email" required placeholder="<?= getTranslation($contact[2], 'email')  ?>*">
        </div>

        <div class="col-md-4 col-12 mb-4">
          <input type="text" id="phone" class="form-control" name="phone" required placeholder="<?= getTranslation($contact[2], 'phone') ?>*">
        </div>

        <div class="col-md-12 col-12 mb-4">
          <textarea class="form-control rounded-0" id="content" rows="5" name="message" required placeholder="<?= getTranslation($contact[2], 'message') ?>*"></textarea>
        </div>

        <div class="col-md-12 col-12 mb-4">
          <div class="custom-control custom-checkbox mb-2">
            <input type="checkbox" name="rodo1" class="custom-control-input" id="rodo1">
            <label class="custom-control-label text-left" for="rodo1">
              <?= getTranslation($settings, 'rodo') ?>
            </label>
          </div>
          <div class="custom-control custom-checkbox">
            <input type="checkbox" name="rodo2" class="custom-control-input" id="rodo2">
            <label class="custom-control-label text-left" for="rodo2">
              <?= getTranslation($settings, 'rodo_tel') ?>
            </label>
          </div>          
        </div>

        <div class="col-md-12 col-12 mb-4 text-center">
          <input type="submit" class="btn btn-secondary btn-submit" value="<?= getTranslation($contact[2], 'button_name') ?> >">
        </div>

      </div>

    </form>

  </div>

</section>

<section class="pb-3 pb-lg-5">

  <div class="container">

    <h2 class="text-center py-3"><?= getTranslation($contact[3], 'title') ?></h2>

    <div class="row">

      <div class="col-md-12 col-12 text-center">
        <a href="<?= base_url().'kontakt' ?>">
          <input type="submit" class="btn btn-secondary btn-submit" value="<?= getTranslation($contact[3], 'button_name') ?>">
        </a>
      </div>

    </div>

  </div>

</section>

</main>
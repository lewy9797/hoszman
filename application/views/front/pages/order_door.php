		<main class="realizations-doors-main">
			<?php if($this->session->flashdata('success')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('success') ?></strong>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('error') ?></strong>                  
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>
			<form id="contact-form" action="<?= base_url() . 'order_doors_mailer/send' ?>" method="POST" enctype="multipart/form-data">
				<section>
					<div class="container">
						<div class="row">
							<div class="col-12 col-lg-12 text-center">
								<div class="text-slider ">
									<h1 class="header-realizations offer-realizations mb-4 mb-lg-5 font-bold"><?= getTranslation($order_attributes[0], 'title')  ?></h1>
									<div class="order-description pt-3">
										<div class="text-justify"><?= getTranslation($order_attributes[0], 'description') ?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">
						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes[0], 'first_title') ?></h2>

						<div class="row mt-4">
							<?php $i=1; foreach($wood_types as $wood_type): ?>
							<div class="col-md-4 col-12 mb-4 text-center">
								<!-- <input class="form-check-input" type="radio" name="woodRadio" id="material<?= $i ?>" value="<?= $wood_type->title ?>"> -->
								<label class="form-check-label" for="material<?= $i ?>">
									<img class="img-fluid" src="<?= base_url(). 'uploads/' . $wood_type->photo ?>" alt="<?= getTranslation($wood_type, 'alt')  ?>">
									<h5 class="text-uppercase pt-3"><?= getTranslation($wood_type, 'title')  ?></h5>
								</label>
							</div>
							<?php $i++; endforeach; ?>


						</div>

					</div>

				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?=  getTranslation($order_attributes[0], 'second_title') ?></h2>

						<div class="row mt-4">
							<?php $i=1; foreach($door_projects as $door_project): ?>
							<div class="col-md-3 col-12 mb-4 text-center">
								<div class="w-100 pattern-border">
									<!-- <input class="form-check-input" type="radio" name="door_project" id="<?= $door_project->title ?>" value="<?= $door_project->title ?>"> -->
									<label class="form-check-label pattern-width" for="pattern<?= $i ?>">
										<img src="<?= base_url() . 'uploads/' . $door_project->photo ?>" alt="<?= getTranslation($door_project, 'alt') ?>">
									</label>
								</div>
								<h5 class="text-uppercase pt-3"><?= $door_project->title ?></h5>
							</div>
							<?php $i++; endforeach; ?>



						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes[0], 'third_title') ?></h2>

						<div class="row mt-4">

							<div class="col-md-5 col-12 params-font">
								<p class="text-uppercase"><span class="font-bold"></span> <?=  getTranslation($door_parameters[0], 'wall_thickness') ?></p>
								<p class="text-uppercase"><span class="font-bold"></span> <?=  getTranslation($door_parameters[0], 'door_width') ?></p>
								<p class="text-uppercase"><span class="font-bold"></span> <?=  getTranslation($door_parameters[0], 'door_height') ?></p>
							</div>

							<div class="col-md-4 col-12">
								<p><img class="w-100" src="<?= base_url() . 'uploads/' . $door_parameters[0]->photo ?>" alt="<?= getTranslation($door_parameters[0], 'alt') ?>"></p>
							</div>

						</div>

					</div>
				</section>


				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h1 class="header-realizations text-center offer-realizations mb-5 mb-lg-5 font-bold"><?= getTranslation($order_attributes[0], 'order_title')  ?></h1>
						<div id="order-patterns">
							<input type="hidden" id="patterns_number" name="patterns_number" value="1">
							<div id="order-pattern-1" class="row mb-4 pt-4 order-pattern d-flex justify-content-between" >
								<div class="col-md-6 col-lg-2 col-12 single-field ">
									<label class="order-pattern-label" for="exampleForm2"><?=  getTranslation($order_attributes[0], 'door_pattern') ?></label>
									<select required id="order_pattern_1" class="browser-default order_pattern_1 custom-select order-pattern-input" name="order_pattern_1">
										<?php foreach($door_projects as $door_project): ?>
											<option value="<?= $door_project->title ?>"><?= $_SESSION['lang'] == 'pl' ? 'Drzwi nr ' : 'Tür Nr ' ?><?= getTranslation($door_project, 'title') ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-6 col-lg-1 col-12 single-field dimensions">
									<label class="order-pattern-label" for="exampleForm2"><?= getTranslation($order_attributes[0], 'order_wall_thickness') ?></label>
									<input required type="number" placeholder="<?= getTranslation($order_attributes[0], 'wall_thickness_unit') ?>" min="0.01" step="0.01" id="wall_thickness_1" class="form-control wall_thickness_1 order-pattern-input" name="wall_thickness_1">
								</div>
								<div class="col-md-6 col-lg-1 col-12 single-field dimensions">
									<label class="order-pattern-label" for="exampleForm2"><?= getTranslation($order_attributes[0], 'order_door_width') ?></label>
									<input required type="number" placeholder="<?= getTranslation($order_attributes[0], 'door_width_unit') ?>" min="0.01" step="0.01" id="door_width_1" class="form-control door_width_1 order-pattern-input" name="door_width_1">
								</div>
								<div class="col-md-6 col-lg-1 col-12 single-field dimensions">
									<label class="order-pattern-label" for="exampleForm2"><?= getTranslation($order_attributes[0], 'order_door_height') ?></label>
									<input required type="number" placeholder="<?= getTranslation($order_attributes[0], 'door_height_unit') ?>" min="0.01" step="0.01" id="door_height_1" class="form-control door_height_1 order-pattern-input" name="door_height_1">
								</div>
								<div class="col-md-4 col-lg-1 col-12 single-field">
									<label class="order-pattern-label" for="exampleForm2"><?= getTranslation($order_attributes[0], 'door_amount') ?></label>
									<input required type="number" min="1" id="door_amount_1" class="form-control door_amount_1 order-pattern-input" placeholder="<?= getTranslation($order_attributes[0], 'door_amount_unit') ?>" name="door_amount_1">
								</div>
								<div class="col-md-4 col-lg-2 col-12 single-field">
									<label class="order-pattern-label" for="exampleForm2"><?=  getTranslation($order_attributes[0], 'wood') ?></label>
									<select required id="wood_type_1" class="browser-default wood_type_1 custom-select order-pattern-input" name="wood_type_1">
										<?php foreach($wood_types as $wood_type): ?>
											<option value="<?= getTranslation($wood_type, 'title') ?>"><?= getTranslation($wood_type, 'title') ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-4 col-lg-2 col-12 single-field">
									<label class="order-pattern-label" for="exampleForm2"><?= getTranslation($order_attributes[0], 'finish') ?></label>
									<select required id="finish_1" class="browser-default finish_1 custom-select order-pattern-input" name="finish_1">
										<?php foreach($stair_finishes as $finish): ?>
											<option value="<?= getTranslation($finish, 'title') ?>"><?= getTranslation($finish, 'title') ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class="col-md-12 col-12 mt-5 text-center">
								<a class="text-dark"><u><h5 class="font-bold" onclick="addOrderPattern()"><?= getTranslation($order_attributes[0], 'pattern_title') ?></h5></u></a>
							</div>
						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h1 class="header-realizations text-center offer-realizations mb-5 mb-lg-5 font-bold"><?= getTranslation($order_attributes[0], 'form_title') ?></h1>

						<p class="text-center"><?= getTranslation($order_attributes[0], 'form_description') ?></p>

						<div class="row pt-5">

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="name" name="name" class="form-control" placeholder="<?= getTranslation($order_attributes[0], 'name') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="email" name="email" class="form-control" placeholder="<?= getTranslation($order_attributes[0], 'email') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="phone" name="phone" class="form-control" placeholder="<?= getTranslation($order_attributes[0], 'phone') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input  type="text" id="city" name="city" class="form-control" placeholder="<?= getTranslation($order_attributes[0], 'city') ?>">
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input  type="text" id="zip_code" name="zip_code" class="form-control" placeholder="<?= getTranslation($order_attributes[0], 'zip_code') ?>">
							</div>

							<div class="col-md-12 col-12 mb-4">
								<textarea class="form-control rounded-0" name="message" id="content" rows="5" placeholder="<?= getTranslation($order_attributes[0], 'message') ?>*" required></textarea>
							</div>

							<div class="col-md-12 col-12 mb-4">
								<div class="custom-control custom-checkbox mb-2">
									<input type="checkbox" name="rodo1" class="custom-control-input" id="rodo1">
									<label class="custom-control-label" for="rodo1"><?= getTranslation($settings, 'rodo') ?>*</label>
								</div>
								<div class="custom-control custom-checkbox ">
									<input type="checkbox" name="rodo2" class="custom-control-input" id="rodo2">
									<label class="custom-control-label" for="rodo2"><?= getTranslation($settings, 'rodo_tel') ?>*</label>
								</div>                  
							</div>

							<div class="col-md-4 col-12 mb-4">
								<button type="button" class="btn btn-outline-dark btn-sm waves-effect input-file-button"><?= getTranslation($order_attributes[0], 'file_button_name') ?></button>
								<input id="file" type="file" name="attachment" class="custom-file-input btn btn-outline-dark btn-sm waves-effect">
							</div>

							<div class="col-md-4 col-12 mb-4 text-center">
								<input type="submit" class="btn btn-secondary btn-submit" value="<?= getTranslation($order_attributes[0], 'form_button_name') ?>">
							</div>

							<div class="col-md-4 col-12 mb-4">
								<p>&nbsp;</p>
							</div>

						</div>

					</div>
				</section>

			</form>

		</main>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design for Bootstrap</title>
  <!-- MDB icon -->
  <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
  <!-- Font Awesome -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Google Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
  <style>
    body{
      font-size: 0.7rem!important;
    }
    .skills{
      margin-top: 400px;
    }
    .header{
      background-color: rgb(0, 61, 116);
      color: white;
      width: 100%;
      /*font-family: Arimo;*/
      font-size: 2.1rem;
      line-height: 6rem;
      font-weight: bold;
    }
    .right{
      /*font-family: Arimo;*/
      padding: 2.4rem;
      color: rgb(54, 61, 73);
      background-color: rgb(244, 244, 244)
    }
    .p-4 {
      padding: 1.5rem!important;
    }
    .mt-2, .my-2 {
      margin-top: .5rem!important;
    }
    .p-3 {
      padding: 1rem!important;
    }
    .col-3 {
      -ms-flex: 0 0 25%;
      flex: 0 0 25%;
      max-width: 25%;
    }
    .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
      width: 100%;
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }
    .row {
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      margin-right: -15px;
      margin-left: -15px;
    }
    .col-9 {
      -ms-flex: 0 0 75%;
      flex: 0 0 75%;
      max-width: 75%;
    }
    .header-text{
      color: rgb(0, 61, 116);
      font-size: 2rem;
      line-height: 1;
      font-weight: bold;
    }
    .title{
      font-size: 1rem;
      line-height: 2.3rem;
      font-weight: bold;
      color: rgb(54, 61, 73);
    }
    .subtitle{
      color: rgb(54, 61, 73);
      font-size: 0.9rem;
      line-height: 2.3rem;
      padding-bottom: 5px;
    }
    .skill-text{
      color: rgb(54, 61, 73);
      font-size: 1.2rem;
      line-height: 2.3rem;
    }
    .fas{
      color: rgb(0, 61, 116)!important;
    }
    .far{
      color: #7a99b4!important;
    }
    .description{
      color: rgb(54, 61, 73);
      font-style: italic;
      font-size: 1rem;
    }
    .left-title{
      font-size: 1.3rem;
      line-height: 2.7rem;
      font-weight: bold;
    }
    .left-subtitle{
      font-size: 1.1rem;
      line-height: 2.5rem;
      font-style: italic;
    }
    .left-p{

    }
    .time{
      font-weight: bold;
      font-size: 1rem;
    }
  </style>
</head>
<body>

  <!-- Start your project here-->  
  <div class="container-fluid">
    <div class="row">
      <div class="header p-1 px-3">
        Daniel Adam Lewicki
      </div>
    </div>
    <div class="row">
      <div class="col-9 p-4">
        <p class="description">Zaangażowany w pracę na stanowisku Junior Web Developer Full Stack, z doświadczeniem w branży programistycznej. Dzięki umiejętnościom interpersonalnym dobrze sprawdzam się w obszarze zarówno backendowym jak i frontendowym. Wiem, jak funkcjonować w dużym zespole, by razem osiągać zamierzone cele. Szukam wyzwań zawodowych w projektowaniu zarówno małych jak i wielkich aplikacji internetowych.</p>
        <div class="header-text mt-2">Doświadczenie</div>
        <hr>
        
        <div class="row">
          <div class="col-3 time">
            2016-04 - 2016-05
          </div>
          <div class="col-9">
            <div class="left-title">Praktyka zawodowa</div>
            <div class="left-subtitle">Qualitrol w Belfaście</div>
            <p class="left-p">Podczas praktyki zawodowej w Irlandii Północnej nauczyłem się pracy w zespole międzynarodowym oraz komunikacji interpersonalnej.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-3 time">
            2017-09 - 2018-03
          </div>
          <div class="col-9">
            <div class="left-title">Elektryk</div>
            <div class="left-subtitle">NExeon Group Polska</div>
            <p class="left-p">Podczas pracy w firmie NExeon zyskałem doświadczenie w pracy w grupie. Zajmowałem się wybranymi budowami, na których należało wykonać instalację elektryczną, system alarmowy lub instalację CCTV na małych i dużych gabarytowo powierzchniach industrialnych.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-3 time">
            2020-01 - obecnie
          </div>
          <div class="col-9">
            <div class="left-title">Junior Web Developer</div>
            <div class="left-subtitle">Ad Awards Sp.j., Legnica</div>
            <p class="left-p">Pracuję na stanowisku Junior Web Developer w trybie Full Stack. Pracuję głównie na frameworku CodeIgniter. Projektuję oraz programuję aplikację internetowe dla firmy Ad Awards oraz firm zewnętrznych.</p>
          </div>
        </div>
        

      </div>
      <div class="col-3 p-3 right h-100">
        <div class="img">
          <img src="<?= base_url().'assets/front/' ?>img/ja.jpg" class="img-fluid" alt="">
        </div>
        <div class="header-text mt-3">Dane Osobowe</div>
        <hr>
        <div class="title">E-mail:</div>
        <div class="subtitle">d.lewicki97@gmail.com</div>
        <div class="title">Number telefonu:</div>
        <div class="subtitle">794 640 515</div>
        <div class="title">Adres:</div>
        <div class="subtitle">Ul. Budowniczych LGOM 9/1 Lubin</div>
        <div class="title">Języki:</div>
        <div class="subtitle">Angielski - B2</div>
        


      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="header-text" style="margin-top: 250px;">Wykształcenie</div>
        <hr>
        <div class="row">
          <div class="col-3 time">
            2008-09 - 2012-06
          </div>
          <div class="col-9">
            <div class="left-title">Państwowa Szkoła Muzyczna I st.</div>
            <div class="left-subtitle">PSM w Lubinie</div>
            <p class="left-p">Podczas nauki w szkole muzycznej nauczyłem się dyscypliny, zarządzania swoim czasem oraz pracy w grupie. Uczęszczałem do klasy fortepianowej.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-3 time">
            2014-09 - 2017-06
          </div>
          <div class="col-9">
            <div class="left-title">Zespół Szkół nr 1</div>
            <div class="left-subtitle">Technik Elektryk</div>
            <p class="left-p">Podczas nauki w technikum elektrycznym, zdobyłem niezbędną wiedzę do wykonywania instalacji elektrycznych oraz konserwacji i eksploatacji silników elektrycznych.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-3 time">
            2017-10 - obecnie
          </div>
          <div class="col-9">
            <div class="left-title">Państwowa Wyższa Szkoła Zawodowa im. Witelona, Legnica</div>
            <div class="left-subtitle">Informatyka - Programowanie aplikacji mobilnych i internetowych</div>
            <p class="left-p">Temat pracy inżynierskiej: System Internetowy wspomagający pracę klubu bilardowego. System napisany został w technologii Laravel z wykorzystaniem REST API oraz Vue.js.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row skills">
      <div class="col p-4">
        <div class="header-text mt-2">Umiejętności</div>
        <hr>
        <div class="d-flex flex-row justify-content-between">
          <div class="skill-text">PHP</div>
          <div class="stars">
           <i class="fas fa-star"></i>
           <i class="fas fa-star"></i>
           <i class="fas fa-star"></i>
           <i class="fas fa-star"></i>
           <i class="far fa-star"></i>
         </div>
       </div>
       <div class="d-flex flex-row justify-content-between">
        <div class="skill-text">CodeIgniter</div>
        <div class="stars">
         <i class="fas fa-star"></i>
         <i class="fas fa-star"></i>
         <i class="fas fa-star"></i>
         <i class="fas fa-star"></i>
         <i class="far fa-star"></i>
       </div>
     </div>
     <div class="d-flex flex-row justify-content-between">
      <div class="skill-text">Laravel</div>
      <div class="stars">
       <i class="fas fa-star"></i>
       <i class="fas fa-star"></i>
       <i class="fas fa-star"></i>
       <i class="far fa-star"></i>
       <i class="far fa-star"></i>
     </div>
   </div>

   <div class="d-flex flex-row justify-content-between">
    <div class="skill-text">Vue.js</div>
    <div class="stars">
     <i class="fas fa-star"></i>
     <i class="fas fa-star"></i>
     <i class="fas fa-star"></i>
     <i class="fas fa-star"></i>
     <i class="far fa-star"></i>
   </div>
 </div>

 <div class="d-flex flex-row justify-content-between">
  <div class="skill-text">OOP</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="far fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">JavaScript</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="far fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">MySQL</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="far fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">CSS3</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">HTML5</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">GIT</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="far fa-star"></i>
   <i class="far fa-star"></i>
 </div>
</div>


<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">Bootstrap 4</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">Vuetify</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
 </div>
</div>

<div class="d-flex flex-row justify-content-between">
  <div class="skill-text">REST API</div>
  <div class="stars">
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="fas fa-star"></i>
   <i class="far fa-star"></i>
   <i class="far fa-star"></i>
 </div>
</div>

</div>
</div>
<div class="row">
  <div class="col-12">
    <div class="header-text mt-5">Cechy Charakteru</div>
    <hr>
    <div class="skill-text">Jestem osobą ambitną o silnym charakterze. Nie poddaję się łatwo. Lubię wyzwania. Uwielbiam uczucie satysfakcji z dobrze wykonanej roboty. Moim celem jest zyskiwanie nowego doświadczenia, aby móc doskonalić swoje umiejętności oraz szlifować słabe strony. Uczę się nowych rzeczy chętnie i sprawia mi to przyjemność.</div>
  </div>
</div>
<div class="row" style="margin-top: 250px;">
  <div class="col-12">
    <div class="header-text mt-2">Zainteresowania</div>
    <hr>
    <div class="skill-text">Poza programowaniem prowadzę zdrowy tryb życia - ćwiczenia fizyczne, zdrowe odżywianie, deskorolka, tenis. Od roku gram także sporadycznie w bilarda. W wieku 12 lat ukończyłem Państwową Szkołę Muzyczną I stopnia w Lubinie na Fortepian - wykorzystuję uzyskaną wiedzę w produkcji muzycznej. Potrafię również grać na gitarze.</div>
  </div>
</div>
</div>
<!-- End your project here-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>

</body>
</html>

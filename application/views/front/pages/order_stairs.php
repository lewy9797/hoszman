		<main class="realizations-doors-main">
			<?php if($this->session->flashdata('success')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('success') ?></strong>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error')):?>
				<div class="col-md-12 mb-5">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong><?= $this->session->flashdata('error') ?></strong>                  
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			<?php endif; ?>

			<section>
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-12 text-center">
							<div class="text-slider ">
								<h1 class="header-realizations offer-realizations mb-4 mb-lg-5 font-bold"><?= getTranslation($order_attributes, 'title') ?></h1>
								<div class="order-description pt-3">
									<p class="text-justify"><?= getTranslation($order_attributes, 'description') ?></p>
								</div>
							</div>
						</div>
					</div>
					<h2 class="text-center py-3 font-bold"><?=  getTranslation($order_attributes, 'info') ?></h2>
				</div>
			</section>

			<form id="contact-form" action="<?= base_url(). 'order_stairs_mailer/send' ?>" method="POST" enctype="multipart/form-data">

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">
						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'first_title') ?></h2>

						<div class="row mt-4">
							<?php foreach($wood_types as $type): ?>
								<div class="col-md-4 col-12 mb-4 text-center">
									<input class="form-check-input" type="radio" name="wood_type" id="<?= getTranslation($type, 'title') ?>" value="<?= $type->title ?>">
									<label class="form-check-label" for="<?= getTranslation($type, 'title')  ?>">
										<img class="img-fluid" src="<?= base_url(). 'uploads/'. $type->photo ?>" alt="<?= getTranslation($type, 'alt') ?>">
										<h5 class="text-uppercase pt-3"><?= getTranslation($type, 'title') ?></h5>
									</label>
								</div>
							<?php endforeach; ?>


						</div>

					</div>

				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'second_title') ?></h2>

						<div class="row mt-4">
							<?php foreach($stair_projects as $project): ?>
								<div class="col-md-3 col-12 mb-4 text-center">
									<input class="form-check-input" type="radio" name="stair_project" id="<?= getTranslation($project, 'title') ?>" value="<?= $project->title ?>">
									<div class="w-100 stairs-pattern">
										<label class="form-check-label pattern-width" for="<?= getTranslation($project, 'title') ?>" >
											<img class="w-100" src="<?= base_url(). 'uploads/'. $project->photo ?>" alt="<?= getTranslation($project, 'alt') ?>">
											<h5 class="text-uppercase pt-3 stair-project-title"><?= getTranslation($project, 'title') ?></h5>
										</label>
									</div>
									
								</div>
							<?php endforeach; ?>
							

						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'third_title') ?></h2>

						<div class="row mt-4">
							<?php foreach($stair_constructions as $construct): ?>
								<div class="col-md-3 col-12 mb-4 text-center">
									<input class="form-check-input" type="radio" name="stair_construction" id="<?= getTranslation($construct, 'title') ?>" value="<?= $construct->title ?>">
									<div class="w-100 stairs-construct">
										<label class="form-check-label stair-construction-photo pattern-width" for="<?= getTranslation($construct, 'title') ?>" style="background-image: url('<?= base_url() . 'uploads/' . $construct->photo ?>')">
											<!-- <img class="w-100" src="<?= base_url() . 'uploads/' . $construct->photo ?>"> -->
										</label>
										<h5 class="text-uppercase pt-3"><?= getTranslation($construct, 'title') ?></h5>
									</div>
								</div>
							<?php endforeach; ?>
							

						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'fourth_title') ?></h2>

						<div class="row">

							<div class="col-md-6 col-12 text-center">
								<h6 class="text-uppercase"><?= getTranslation($stair_parameters[0], 'title') ?></h6>
								<img class="w-100" src="<?= base_url(). 'uploads/'. $stair_parameters[0]->photo ?>" alt="<?= getTranslation($stair_parameters[0], 'alt') ?>">

								<div class="row mb-4 m-md-0 m-lg-0">
									<div class="col-md-6 col-12 text-center">
										<div class="text-uppercase stair-text"><?= getTranslation($stair_parameters[0], 'first_title') ?></div>

										<label for="ceiling_height" class="text-uppercase stair-text"><?= getTranslation($stair_parameters[0], 'first_description') ?></label>
										<input type="number" placeholder="<?= getTranslation($order_attributes, 'ceiling_height_unit') ?>" step="0.01" min="0.01" id="ceiling_height" name="ceiling_height" class="form-control">
									</div>
									<div class="col-md-6 col-12 text-center">
										<div class="text-uppercase stair-text"><?= getTranslation($stair_parameters[0], 'second_title') ?></div>

										<label for="overall_height" class="text-uppercase stair-text"><?= getTranslation($stair_parameters[0], 'second_description') ?></label>
										<input type="number" placeholder="<?= getTranslation($order_attributes, 'overall_height_unit') ?>" step="0.01" min="0.01" id="overall_height" name="overall_height" class="form-control">
									</div>
								</div>
							</div>

							<div class="col-md-6 col-12 text-center d-flex flex-column justify-content-between">
								<h6 class="text-uppercase"><?= getTranslation($stair_parameters[1], 'title') ?></h6>
								<img class="w-100" src="<?= base_url(). 'uploads/'. $stair_parameters[1]->photo ?>" alt="<?= getTranslation($stair_parameters[1], 'alt') ?>">

								<div class="row">
									<div class="col-md-12 col-12 text-center">
										<div class="text-uppercase stair-text"><?= getTranslation($stair_parameters[1], 'first_title')  ?></div>

										<label for="stair_width" class="text-uppercase stair-text"><?=  getTranslation($stair_parameters[1], 'first_description') ?></label>
										<input type="number" placeholder="<?= getTranslation($order_attributes, 'stair_width_unit') ?>" step="0.01" min="0.01" id="stair_width" name="stair_width" class="form-control w-50 stair-width">
									</div>
								</div>
							</div>

						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h5 class="text-center"><?= getTranslation($order_attributes, 'lifts') ?></h5>
						<div class="row">
							<div class="col-md-12 col-12 pt-3">
								<input type="number" min="1" id="lifts" name="lifts" class="form-control w-50 stair-width">
							</div>
						</div>
					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'fifth_title') ?></h2>

						<div class="row">
							<?php foreach($step_fulfillments as $fulfillment): ?>
								<div class="col-md-4 col-12">
									<input class="form-check-input" type="radio" name="step_fulfillment" id="<?= getTranslation($fulfillment, 'title') ?>" value="<?= $fulfillment->title ?>">
									<div class="w-100 stairs-fullfill">
										<label class="form-check-label step-fulfillment-photo pattern-width" for="<?= getTranslation($fulfillment, 'title') ?>" style="background-image: url('<?= base_url(). 'uploads/'. $fulfillment->photo ?>')">
											<!-- <img class="w-100" src="<?= base_url(). 'uploads/'. $fulfillment->photo ?>"> -->
										</label>
										<h5 class="text-uppercase text-center "><?= getTranslation($fulfillment, 'title') ?></h5>
									</div>
								</div>
							<?php endforeach; ?>

							
						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'sixth_title') ?></h2>

						<div class="row">
							<?php $i=0; foreach($balustrade_fulfillments as $fulfillment): ?>
							<div class="col-md-5-own col-12 text-center">
								<input class="form-check-input" type="radio" name="balustrade_fulfillment" id="<?= getTranslation($fulfillment, 'title') ?>" value="<?= $fulfillment->title ?>">
								<div class="w-100 stairs-balu">
									<label class="form-check-label fulfillment-photo pattern-width px-2" for="<?= getTranslation($fulfillment, 'title') ?>" style="background-image: url('<?= base_url(). 'uploads/'. $fulfillment->photo ?>')">
										<!-- <img class="w-100" src="<?= base_url(). 'uploads/'. $fulfillment->photo ?>"> -->
									</label>
									<h5 class="text-uppercase "><?= getTranslation($fulfillment, 'title') ?></h5>
								</div>
								<?php if($i == 0): ?>
									<a href="#" data-toggle="modal" data-target="#pattern-modal" class="btn btn-secondary btn-submit btn-sm"><?= getTranslation($order_attributes, 'pattern_button_name') ?></a>
								<?php endif; ?>
							</div>
							<?php $i++; endforeach; ?>
							
						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<div class="row">
							<div class="col-lg-6 col-12 text-center">
								<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'seventh_title') ?></h2>

								<p class="pt-3"><img src="<?= base_url(). 'uploads/'. $upper_balustrade_length->photo ?>" alt="<?= getTranslation($upper_balustrade_length, 'alt') ?>" class="w-100"></p>

								<label for="upper_balustrade_length" class="text-uppercase stair-text pt-4"><?= getTranslation($upper_balustrade_length, 'label') ?></label>
								<input type="number" placeholder="<?= getTranslation($order_attributes, 'upper_balustrade_length_unit') ?>" step="0.01" min="0.01" id="upper_balustrade_length" name="upper_balustrade_length" class="form-control w-50 stair-width">
							</div>
							<div class="col-lg-6 col-12 text-center">
								<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'eighth_title') ?></h2>

								<p class="pt-3"><img src="<?= base_url(). 'uploads/'. $roof_covering->photo ?>" class="w-100" alt="<?= getTranslation($roof_covering, 'alt') ?>"></p>

								<label for="roof_covering" class="text-uppercase stair-text"><?= getTranslation($roof_covering, 'label') ?></label>
								<input type="number" placeholder="<?= getTranslation($order_attributes, 'roof_covering_unit') ?>" step="0.01" min="0.01" id="roof_covering" name="roof_covering" class="form-control w-50 stair-width">
							</div>
						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="text-center py-3 font-bold"><?= getTranslation($order_attributes, 'ninth_title') ?></h2>

						<div class="row">
							<?php foreach($stair_finishes as $finish): ?>
								<div class="col-md-5-own col-12 mx-auto text-center">
									<input class="form-check-input" type="radio" name="stair_finish" id="<?= getTranslation($finish, 'title') ?>" value="<?= $finish->title ?>">
									<div class="w-100 stairs-paint">
										<label class="form-check-label pattern-width px-2" for="<?= getTranslation($finish, 'title') ?>">
											<img class="w-100 img-fluid finish-img" alt="<?= getTranslation($finish, 'alt') ?>" src="<?= base_url(). 'uploads/'.  $finish->photo ?>">
										</label>
										<h5 class="text-uppercase"><?= getTranslation($finish, 'title') ?></h5>
									</div>
								</div>
							<?php endforeach; ?>
							
						</div>

					</div>
				</section>

				<section class="pt-3 pt-lg-5 pb-2 pb-lg-4">
					<div class="container">

						<h2 class="header-realizations text-center offer-realizations mb-5 mb-lg-5 font-bold"><?= getTranslation($order_attributes, 'form_title') ?></h2>

						<p class="text-center"><?= getTranslation($order_attributes, 'form_description')  ?></p>

						<div class="row pt-5">

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="name" name="name" class="form-control" placeholder="<?= getTranslation($order_attributes, 'name') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="email" name="email" class="form-control" placeholder="<?= getTranslation($order_attributes, 'email') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="phone" name="phone" class="form-control" placeholder="<?= getTranslation($order_attributes, 'phone') ?>*" required>
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="city" name="city" class="form-control" placeholder="<?= getTranslation($order_attributes, 'city') ?>">
							</div>

							<div class="col-md-4 col-12 mb-4">
								<input type="text" id="zip_code" name="zip_code" class="form-control" placeholder="<?= getTranslation($order_attributes, 'zip_code') ?>">
							</div>

							<div class="col-md-12 col-12 mb-4">
								<textarea class="form-control rounded-0" id="content" rows="5" placeholder="<?= getTranslation($order_attributes, 'message') ?>*" name="message" required></textarea>
							</div>

							<div class="col-md-12 col-12 mb-4">
								<div class="custom-control custom-checkbox mb-2">
									<input type="checkbox" name="rodo1" class="custom-control-input" id="rodo1">
									<label class="custom-control-label" for="rodo1"><?= getTranslation($settings, 'rodo') ?>*</label>
								</div>
								<div class="custom-control custom-checkbox ">
									<input type="checkbox" name="rodo2" class="custom-control-input" id="rodo2">
									<label class="custom-control-label" for="rodo2"><?= getTranslation($settings, 'rodo_tel') ?>*</label>
								</div>                 
							</div>

							<div class="col-md-4 col-12 mb-4">
								<button type="button" class="btn btn-outline-dark btn-sm waves-effect input-file-button"><?= getTranslation($order_attributes, 'file_button_name') ?></button>
								<input id="file" type="file" name="attachment" class="custom-file-input btn btn-outline-dark btn-sm waves-effect">
							</div>

							<div class="col-md-4 col-12 mb-4 text-center">
								<input type="submit" class="btn btn-secondary btn-submit" value="<?= getTranslation($order_attributes, 'form_button_name') ?>">
							</div>

							<div class="col-md-4 col-12 mb-4">
								<p>&nbsp;</p>
							</div>

						</div>

					</div>
				</section>

			</form>
			<div class="modal fade" id="pattern-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			aria-hidden="true">
			<div class="modal-dialog modal-fluid" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12 col-lg-6 mb-3" >
								<h2 class="text-center pb-2 font-bold pattern-title"><?= getTranslation($patterns[0], 'title') ?></h2>
								<div class="modal-photo" style="background-image: url('<?= base_url(). 'uploads/'. $patterns[0]->photo ?>')"></div>
								<!-- <img class="img-fluid" src="<?= base_url(). 'uploads/'. $patterns[0]->photo ?>" alt="<?= $patterns[0]->alt ?>"> -->
							</div>
							<div class="col-12 col-lg-6 ">
								<h2 class="text-center pb-2 font-bold pattern-title"><?= getTranslation($patterns[1], 'title') ?></h2>
								<div class="modal-photo" style="background-image: url('<?= base_url(). 'uploads/'. $patterns[1]->photo ?>')"></div>
								<!-- <img class="img-fluid" src="<?= base_url(). 'uploads/'. $patterns[1]->photo ?>" alt="<?= $patterns[1]->alt ?>"> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
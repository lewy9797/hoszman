<main class="realizations-doors-main">

  <section>
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 d-flex flex-column justify-content-end">
          <div class="text-slider ">
            <h1 class="header-realizations mb-4 mb-lg-5"><?= getTranslation($realization, 'title') ?></h1>
            <div class="order-description">
              <?=  getTranslation($realization, 'description') ?>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 py-3 py-lg-0">
          <picture>
            <source data-srcset="<?= base_url().'uploads/'.$realization->photo2 ?>.webp" type="image/webp" class="lazy img-fluid">
              <source data-srcset="<?= base_url().'uploads/'.$realization->photo ?>" type="image/jpeg" class="lazy img-fluid"> 
                <img data-src="<?= base_url() . 'uploads/' . $realization->photo2 ?>" class="lazy img-fluid" alt="<?= getTranslation($realization, 'alt2') ?>">
              </picture>
            </div>
          </div>
        </div>
      </section>

      <section class="py-3 py-lg-5">
        <div class="container">
          <h2 class="text-center py-3"><span class="font-weight-bold"><?= getTranslation($realization_attributes, 'title') ?></span> - <?= getTranslation($realization, 'title') ?></h2>
          <div class="row justify-content-center pb-5 pt-3">
            <?php $i=1; foreach($realization_types as $type): ?>
            <button onclick="getPhotos(<?= $type->id ?>)" id="<?= $type->id ?>" class="custom-btn-2 px-3 mb-4 type-button <?= 'type-button-'.$type->id ?> <?php if($i != count($realization_types)) echo 'border-right '; if($i==1) echo 'active'; ?>"><?= getTranslation($type, 'title') ?></button>
            <?php $i++; endforeach; ?>

          </div>
          <?php $i=1; foreach($realization_types as $type): ?>
          <div class="row py-0 py-lg-3 text-center <?php if($i != 1 ) echo 'd-none'; ?> realization-gallery" id="<?= $type->id ?>">
            <?php $j=1; foreach($single_realizations as $realization): ?>
            <?php if($realization->realization_type_id == $type->id): ?>
              <a class="example-image-link col-12 col-lg-4 p-3 " href="<?= base_url(). 'realizacje/'. $this->uri->segment(2). '/'. $realization->id ?>" >
                <div class="realization-photo lazy" style="background-image: url(<?= base_url(). 'uploads/'. $realization->photo  ?>)" data-bg="<?= base_url(). 'uploads/'. $realization->photo  ?>"></div>
                <!-- <picture>
                  <source src="<?= base_url() . 'uploads/' . $realization->photo ?>.webp" type="image/webp" class="example-image lazy img-fluid">
                    <source src="<?= base_url() . 'uploads/' . $realization->photo ?>" type="image/jpeg" class="example-image lazy img-fluid">
                      <img src="<?= base_url() . 'uploads/' . $realization->photo ?>" class="example-image lazy img-fluid" alt="<?= $realization->alt ?>" id="<?= 'type_id='. $realization->realization_type_id. ', title='. $realization->title ?>" >
                    </picture > -->
                  </a>
                <?php endif; ?>
                <?php $j++;  endforeach; ?>
              </div>
              <?php $i++; endforeach; ?>
            </div>
          </section>


        </main>
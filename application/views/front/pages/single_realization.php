<main class="realizations-doors-main">
	<section class="slider">
		<div class="container mb-2">
			<h1 class="header-realizations offer-realizations text-center mb-4 mb-lg-5 font-bold"><?= getTranslation($single_realization, 'title')  ?></h1>
			<div class="owl-carousel owl-theme owl-single-realization">
				<?php foreach($gallery as $photo): ?>
					<div class="item slider-realization-photo" data-bg="<?= base_url().'uploads/'.$photo->photo ?>" style="background-image: url(<?= base_url().'uploads/'.$photo->photo ?>)">
						<!-- <img data-src="<?= base_url().'uploads/'.$photo->photo ?>" alt="<?= getTranslation($photo, 'alt') ?>" class="lazy img-fluid"> -->
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row d-flex justify-content-center">
				<?php foreach($gallery as $photo): ?>
					<a class="example-image-link col-12 col-lg-4 p-3 " href="<?= base_url(). 'uploads/'. $photo->photo  ?>" data-lightbox="image" >
						<div class="realization-photo lazy" style="background-image: url(<?= base_url(). 'uploads/'. $photo->photo  ?>)" data-bg="<?= base_url(). 'uploads/'. $photo->photo  ?>"></div>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
</main>
<header>

  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container d-flex justify-content-between align-items-center f-column">
      <a class="navbar-brand" href="<?= base_url() ?>">
        <picture>
          <source data-srcset="<?= base_url().'uploads/'.$settings->logo ?>" type="image/png" class="lazy img-fluid">
            <source data-srcset="<?= base_url().'uploads/'.$settings->logo ?>" type="image/png" class="lazy img-fluid"> 
              <img data-src="<?= base_url() . 'uploads/' . $settings->logo ?>" alt="logo" class="lazy">
            </picture>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="w-100 d-flex justify-content-between py-0 py-lg-3">
          <div class="collapse navbar-collapse w-100 justify-content-between" id="navbarNav">
            <ul class="navbar-nav mx-auto">
              <?php foreach($subpages as $subpage): ?>

                <?php if($subpage->list): ?>
                  <li class="nav-item d-flex align-items-center  dropdown">
                    <a class="nav-link text-center dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><?= getTranslation($subpage, 'title') ?></a>
                    <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="<?= base_url(). $subpage->page. '/drzwi' ?>"><?= $_SESSION['lang'] == 'pl' ? 'drzwi' : 'TÜREN'; ?></a>
                      <a class="dropdown-item" href="<?= base_url(). $subpage->page. '/schody' ?>"><?= $_SESSION['lang'] == 'pl' ? 'schody' : 'TREPPEN'; ?></a>
                    </div>
                  </li>
                  <?php elseif($subpage->page == 'home'): ?>
                    <li class="nav-item d-flex align-items-center  dropdown">
                    <a class="nav-link text-center dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><?= getTranslation($subpage, 'title') ?></a>
                    <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="<?= base_url(). 'o-firmie' ?>"><?= getTranslation($subpages[6], 'title') ?></a>
                      <a class="dropdown-item" href="<?= base_url().  'wspolpraca' ?>"><?= getTranslation($subpages[7], 'title') ?></a>
                    </div>
                  </li>

                    <?php elseif($subpage->page != 'o-firmie' && $subpage->page != 'wspolpraca'): ?>
                      <li class="nav-item d-flex align-items-center <?php if ($subpage->page == $current_page->page) echo 'active' ?>">
                      <a class="nav-link text-center" href="<?= base_url(). $subpage->page ?>" title="link"><?php if($subpage->page == '') echo $subpage->photo; else echo getTranslation($subpage, 'title') ?></a>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
              <div class="languages d-flex">
                <a href="" class="img-fluid px-1" data-lang="pl" onclick="changeLang('pl');">
                  <img src="<?= base_url() ?>assets/front/img/Prostokąt 17.png" alt="polski" >
                </a>
                <a href="" class="img-fluid px-1" data-lang="de" onclick="changeLang('de');">
                  <img src="<?= base_url() ?>assets/front/img/Prostokąt 17 kopia.png" alt="niemiecki" ></a>
                </div>
              </div>

            </div>
          </div>
        </nav>
      </header>
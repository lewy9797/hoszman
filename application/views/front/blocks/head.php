<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?= $current_page->meta_description; ?>">
  <meta name="keywords" content="<?= $current_page->keywords; ?>">
  <title><?= $settings->meta_title ?><?php if($current_page->meta_title != '') echo ' - '.  $current_page->meta_title; ?></title>
  <!-- Font Awesome -->
  <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" async>
  <!-- Bootstrap core CSS -->
  <link href="<?= base_url() ?>assets/front/css/bootstrap.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async>
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url() ?>assets/front/css/mdb.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async>
  <!-- Your custom styles (optional) -->
  <link href="<?= base_url() ?>assets/front/css/style.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" >
  <link href="<?= base_url() ?>assets/front/owlcarousel/owl.theme.default.min.css" rel="stylesheet"  >
  <link href="<?= base_url() ?>assets/front/owlcarousel/owl.carousel.min.css" rel="stylesheet" >
  <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="<?= base_url() ?>assets/front/css/lightbox.min.css" async>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async>
  <link href="https://fonts.googleapis.com/css2?family=Caveat&display=swap" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async>
  <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" defer/>
  
</head>
<style>
  .no-webp{
    display: none;
  }
</style>
<body>
<footer>
  <div class="container pt-5">
    <div class="row pb-3 ">
      <div class="col-8 col-md-6 mx-auto py-3">
        <picture>
          <source data-srcset="<?= base_url().'uploads/'.$settings->logo ?>.webp" type="image/webp" class="lazy img-fluid">
            <source data-srcset="<?= base_url().'uploads/'.$settings->logo ?>" type="image/jpeg" class="lazy img-fluid"> 
              <img data-src="<?= base_url() . 'uploads/' . $settings->logo ?>" alt="logo" class="">
            </picture>
            <ul>
              <a style="height:48px" class="footer-link" href="https://www.google.com/maps/place/Hoszman.+Schody+i+drzwi/@51.5571783,15.5256733,17z/data=!4m13!1m7!3m6!1s0x4708ac3bfcebaf39:0x2f8a2578b835517a!2s<?= $contact_settings->address ?>,+<?= $contact_settings->zip_code ?>+<?= $contact_settings->city ?>!3b1!8m2!3d51.557175!4d15.527862!3m4!1s0x4708ac3bfcebaf39:0xcf11718392052b03!8m2!3d51.557175!4d15.527862" title="address"><li><?= $contact_settings->address ?></li>
                <li><?= $contact_settings->zip_code . ' ' . $contact_settings->city ?></li></a>
                <li><a class="footer-link" href="tel: <?= $contact_settings->phone1 ?>" title="phone">Tel. <?= $contact_settings->phone1 ?></a></li>
                <li><a class="footer-link" href="tel: <?= $contact_settings->phone2 ?>" title="phone">Tel. kom. <?= $contact_settings->phone2 ?></a></li>
                <li><a class="footer-link" href="mailto: <?= $contact_settings->email1 ?>" title="mail"><?= $contact_settings->email1 ?></a></li>
              </ul>
            </div>
            <div class="col-12 col-md-6 d-flex justify-content-start align-items-center flex-column py-3">
              <h3 class="heading-white-footer"><?= getTranslation($footer, 'social_media_title') ?></h3>
              <div class="d-flex">
                <?php if($settings->fb_link): ?>
                  <a href="<?= $settings->fb_link ?>"><img src="<?= base_url() ?>assets/front/img/fb.png" alt="facebook" class="px-2 img-fluid"></a>
                <?php endif; if($settings->inst_link): ?>

                <a href="<?= $settings->inst_link ?>"><img src="<?= base_url() ?>assets/front/img/insta.png" alt="instagram" class="px-2 img-fluid"></a>
              <?php endif; if($settings->yt_link): ?>
              <a href="<?= $settings->yt_link ?>"><img src="<?= base_url() ?>assets/front/img/yt.png" alt="youtube" class="px-2 img-fluid"></a>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="row border-top copyrights justify-content-between py-4 m-0">
        <div class="">HOSZMAN 2020 © Wszystkie prawa zastrzeżone</div>
        <div>Projekt i wdrożenie: <a href="https://agencjamedialna.pro.com" title="strona wykonująca">AdAwards</a></div>
      </div>
    </div>
  </footer>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/popper.min.js" async></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/bootstrap.min.js" async></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/mdb.min.js" async></script>
  <?php if($this->uri->segment(1) == 'wycena'): ?>
    <script type="text/javascript" src="<?= base_url() ?>assets/front/js/order/order_door.js" async></script>
  <?php endif; ?>
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/order/OrderPattern.js" async></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/front/js/realization/realization.js" async></script>
  <script src="<?= base_url() ?>assets/front/js/lc_lightbox.lite.js" async></script>
  <script src="<?= base_url() ?>assets/front/js/lightbox-plus-jquery.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?render=<?= $settings->captcha ?>" async></script>
  <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/front/owlcarousel/owl.carousel.min.js" ></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js" async></script>
  <script src="https://www.google.com/recaptcha/api.js" async></script>
  <script type="text/javascript"> links = document.querySelectorAll("link"); for (var i = links.length - 1; i >= 0; i--) { if(links[i].getAttribute('rel') == 'preload'){ links[i].setAttribute("rel", "stylesheet"); } } </script>
  <script type="text/javascript">
    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
        0:{
          items:1
        },
      }
    })
  </script>

  <script>
    var matchingElements = [];
    var allElements = document.getElementsByTagName('*');
    for (var i = 0, n = allElements.length; i < n; i++)
    {
      if (allElements[i].getAttribute('data-src') !== null)
      {
        document.createAttribute('src');
        allElements[i].src = allElements[i].getAttribute('data-src');
      }
    }
  </script>
  <script type="text/javascript">

    function changeLang(lang){
      $.ajax({
        url:"<?php echo base_url(); ?>home/change/"+lang+"",
        success: function () {
          location.reload();
        }
      });
    }

  </script>
  <!-- <?php if(isset($_SESSION['lang'])): ?>
    <?php if($_SESSION['lang'] != 'pl'): ?>
      <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({pageLanguage: 'pl', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
        }
        jQuery('.lang-select').click(function() {
          var theLang = jQuery(this).attr('data-lang');
          jQuery('.goog-te-combo').val(theLang);
          window.location = jQuery(this).attr('href');
          location.reload();
        });
      </script>
    <?php endif; ?>
  <?php endif; ?> -->

  <!-- <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
  <script>



    var lazyLoadInstance = new LazyLoad({
      elements_selector: ".lazy"
      // ... more custom settings?
    });
    function hasWebP() {
      var rv = $.Deferred(), img = new Image();
      img.onload = function() { rv.resolve(); };
      img.onerror = function() { rv.reject(); };
      img.src = "https://www.gstatic.com/webp/gallery/1.webp";
      return rv.promise();
    }

    hasWebP().then(function() {
      nowebp = document.getElementsByClassName('no-webp');
      for(i = 0; i < nowebp.length; i++) {
        nowebp[i].style.display = "none";
      }
      console.log("Hooray!!  WebP is enabled.  Things will be wonderful now.");
    }, function() {

      webp = document.getElementsByClassName('webp');
      for(i = 0; i < webp.length; i++) {
        webp[i].style.display = "none";
      }
      nowebp = document.getElementsByClassName('no-webp');
      for(i = 0; i < nowebp.length; i++) {
        nowebp[i].style.display = "block";
      }
      console.log("Note: your browser does not support the new Google WebP format. Please remain where you are while our support team locates you to begin the reeducation process.");
    });

  </script>
  <script>
    window.addEventListener("load", function(){
      window.cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "black",
            "text": "#fff"
          },
          "button": {
            "background": "#eee",
            "text": "#1C2331!important"
          }
        },
        "type": "opt-out",
        "content": {
          "message": "Nasza strona internetowa korzysta z plików cookie. Dzięki temu możemy zapewnić naszym użytkownikom satysfakcjonujące wrażenia z przeglądania naszej witryny i jej prawidłowe funkcjonowanie.",
          "dismiss": "Rozumiem",
          "deny": "",
          "allow": "Rozumiem",
          "link": "Czytaj więcej...",
          "href": "<?php echo base_url(); ?>uploads/<?= $settings->privace;  ?>"
        }
      })});
    </script>
    <?php if($this->uri->segment(1) == 'kontakt' || $this->uri->segment(1) == 'zamowienie'): ?>

    <script>
      $('#contact-form').submit(function(event) {
        event.preventDefault();
        var email = $('#email').val();

        grecaptcha.ready(function() {
          grecaptcha.execute('<?= $settings->captcha ?>', {action: 'mailer/send'}).then(function(token) {
            $('#contact-form').prepend('<input type="hidden" name="secret_key" value="<?= $settings->captcha_secret ?>">');
            $('#contact-form').unbind('submit').submit();
          });;
        });
      });
    </script>
  <?php endif; ?>


</script>
</body>

</html>
